<?php

class MY_Controller extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->init();
        $this->auth_user();
        $this->auth_pesky();
        $this->lang();
    }

    function init() {
        $this->configs();
        $this->global_variable();
        $this->template_configs();
        $this->load_ajax_var();
        $this->load_ajax_func();
        $this->auth_config();
        $this->_get_notif();
        $this->_get_msg();
        $this->_get_task();
        if ($this->template_configs->_module != 'backend') {
            $this->get_footer();
        }
        /*
         * Config
         */
        $arr = array();
        if ($this->config) {
            foreach ($this->config AS $key => $val) {
                if (!is_array($val)) {
                    $arr[] = 'var ' . $key . ' = "' . $val . '";';
                }
            }
        }
        //set this to vars
        $ajax_var = '';
        foreach ($arr AS $key => $val) {
            if (!empty($ajax_var))
                $ajax_var .= ' ';
            $ajax_var .= $val;
        }
        $this->load->vars('_ajax_var_configs', $ajax_var);

        /*
         * Template
         */
        $arr2 = array();
        if ($this->template_configs) {
            foreach ($this->template_configs AS $key => $val) {
                if (!is_array($val)) {
                    $arr2[] = 'var ' . $key . ' = "' . $val . '";';
                }
            }
        }
        //set this to vars
        $ajax_var2 = '';
        foreach ($arr2 AS $key => $val) {
            if (!empty($ajax_var2))
                $ajax_var2 .= ' ';
            $ajax_var2 .= $val;
        }
        $this->load->vars('_var_template', $this->template_configs);
        $this->load->vars('_ajax_var_template', $ajax_var2);

        //load var for menu
        if ($this->template_configs->_module == 'backend') {
            $module = $this->config->session_name;
        } elseif ($this->template_configs->_module == 'pesky') {
            $module = $this->config->session_pesky;
        }

//        $sess = $this->_session_auth($module);
//        if ($sess['is_logged_in']) {
//            $mn = $this->menu($this->get_module_id(), false, 1);
//        } else {
//            $mn = $this->menu($this->get_module_id(), false, 0);
//        }

        if (isset($mn) && !empty($mn)) {
            $this->load->vars('_menu', $mn);
        }
    }

    public function configs() {
        $file_conf = $this->config->config;
        $arr = array();
        if ($file_conf) {
            foreach ($file_conf AS $key => $val) {
                $arr[$key] = $val;
            }
        }
        $this->load->model(array('Tbl_configs'));
        $result = $this->Tbl_configs->find('all', array('conditions' => array('is_static' => 0, 'is_active' => 1)));
        if ($result) {
            foreach ($result AS $key => $val) {
                $arr{$val['keyword']} = $val['value'];
            }
        }
        //unset($this->config->config);
        if (isset($arr) && !empty($arr)) {
            foreach ($arr AS $key => $val) {
                $this->config->{$key} = $val;
            }
        }
    }

    public function global_variable() {
        $this->load->model(array('Tbl_global_variables'));
        $result = $this->Tbl_configs->find('all', array('conditions' => array('is_active' => 1)));
        $arr = array();
        if ($result) {
            foreach ($result AS $key => $val) {
                $arr{$val['keyword']} = $val['value'];
            }
        }
        $this->global_variables = new stdClass();
        if (isset($arr) && !empty($arr)) {
            foreach ($arr AS $key => $val) {
                $this->global_variables->{$key} = $val;
            }
        }
    }

    protected function template_configs() {
        $arr = array(
            '_module' => $this->router->fetch_module(),
            '_class' => $this->router->fetch_class(),
            '_action' => $this->router->fetch_method(),
            '_directory' => $this->_get_module_dir(),
            '_view_html' => $this->_get_module_dir() . '/' . $this->router->fetch_class() . '/' . $this->router->fetch_method() . '.html.php',
            '_view_js' => $this->_get_module_dir() . '/' . $this->router->fetch_class() . '/' . $this->router->fetch_method() . '.js.php',
            '_app_js' => 'libs/app.js.php',
            '_global_js' => 'libs/global.js.php'
        );
        $this->template_configs = new stdClass();
        if (isset($arr) && !empty($arr)) {
            foreach ($arr AS $key => $val) {
                $this->template_configs->{$key} = $val;
            }
        }
    }

    protected function _get_module_dir() {
        $path = $this->router->fetch_directory();
        if ($path != null) {
            $first = str_replace('..', '', $path);
            $second = explode('/', $first);
            $return = array();
            if ($second) {
                foreach ($second AS $key => $val) {
                    if ($val != '' && $val != 'modules' && $val != 'controllers' && $val != $this->router->fetch_module() && $val != $this->router->fetch_class() && $val != $this->router->fetch_method()) {
                        $return[] = $val;
                    }
                }
            }
            return implode('/', $return);
        } else {
            return null;
        }
    }

    public function load_ajax_var($data = array()) {
        $arr = '';
        if ($data) {
            foreach ($data AS $key => $val) {
                if (!empty($arr))
                    $arr .= ' ';
                if (is_array($val['value'])) {
                    $arr .= 'var ' . $val['keyword'] . ' = "' . json_encode($val['value']) . '";';
                } else {
                    $arr .= 'var ' . $val['keyword'] . ' = "' . $val['value'] . '";';
                }
            }
        }
        $this->load->vars('_load_ajax_var', $arr);
    }

    public function load_ajax_func($data = array()) {
        $this->load->model('Tbl_ajax_funcs');
        $data = $this->Tbl_ajax_funcs->find('all', array('conditions' => array('is_active' => 1)));
        $arr = "";
        if ($data) {
            foreach ($data AS $key => $val) {
                if (!empty($arr))
                    $arr .= ' ';
                $arr .= $val['value'];
            }
        }
        $this->load->vars('_load_ajax_func', $arr);
    }

    public function load_js($path = array()) {
        if ($path) {
            $arr = "";
            foreach ($path AS $key => $val) {
                if (!empty($arr))
                    $arr .= '<br>';
                $arr .= '<script src="' . $val . '" type="text/javascript"></script>';
            }
        }
        $this->load->vars('_load_js', $arr);
    }

    public function load_css($path = array()) {
        if ($path) {
            $arr = '';
            foreach ($path AS $key => $val) {
                if (!empty($arr))
                    $arr .= '<br>';
                $arr .= '<link href="' . $val . '" rel="stylesheet" type="text/css" />';
            }
        }
        $this->load->vars('_load_css', $arr);
    }

    public function menu($id = null, $is_ajax = true, $is_logged_in = 0, $module_nm = '') {
        $this->load->model(array('Tbl_menus'));
        if ($module_nm == '') {
            $module_nm = $this->template_configs->_module;
        }
        $add_cond = array('a.is_active' => 1, 'a.module_id' => $id, 'a.level' => 1, 'a.is_logged_in' => $is_logged_in);
        $menus = $this->Tbl_menus->find('all', array(
            'fields' => array('a.id root_id', 'a.name text', 'a.path root_path', 'a.icon root_icon', 'a.rank', 'a.is_logged_in', 'a.module_id'),
            'conditions' => $add_cond,
            'order' => array('key' => 'a.rank', 'type' => 'ASC')
                )
        );
        $arr_menu = array();
        if (isset($menus) && !empty($menus)) {
            foreach ($menus AS $key => $value) {
                $childmenus = $this->Tbl_menus->find('all', array(
                    'fields' => array('a.id root_id', 'a.name text', 'a.path root_path', 'a.parent_id foreign_id', 'a.icon root_icon', 'a.rank'),
                    'conditions' => array('a.is_active' => 1, 'a.parent_id' => $value['root_id'], 'a.level' => 2)
                        )
                );
                $arr_menu2 = array();
                if (isset($childmenus) && !empty($childmenus)) {
                    foreach ($childmenus AS $k => $val) {
                        $grandchildmenus = $this->Tbl_menus->find('all', array(
                            'fields' => array('a.id root_id', 'a.name text', 'a.path root_path', 'a.parent_id foreign_id', 'a.icon root_icon', 'a.rank'),
                            'conditions' => array('a.is_active' => 1, 'a.parent_id' => $val['root_id'], 'a.level' => 3)
                                )
                        );
                        $mrg_val3 = array();
                        if (isset($grandchildmenus) && !empty($grandchildmenus)) {
                            foreach ($grandchildmenus AS $j => $v) {
                                $mrg_val3[] = array_merge($v, array('level' => 3));
                            }
                        }
                        $mrg_val2 = array_merge(array_merge($val, array('level' => 2)), array('nodes' => $mrg_val3));
                        $arr_menu2[] = $mrg_val2;
                    }
                }
                $mrg_val = array_merge(array_merge($value, array('level' => 1)), array('nodes' => $arr_menu2));
                $arr_menu[] = $mrg_val;
            }
        }
        $rrr = array();
        if (isset($arr_menu) && !empty($arr_menu)) {
            $root = array(
                'id' => 0,
                'level' => 0,
                'text' => $module_nm
            );
            $r = array_merge($root, array('nodes' => $arr_menu));
            if ($is_ajax == true) {
                $rrr = '[' . json_encode($r) . ']';
            } else {
                $rrr = $r;
            }
        }
        if ($is_ajax == true && !is_array($rrr)) {
            echo $rrr;
        } else {
            return $rrr;
        }
    }

    public function get_module_id() {
        $this->load->model('Tbl_modules');
        return $this->Tbl_modules->get_id($this->template_configs->_module);
    }

    public function _session_auth($sess_name = null) {
        $sess = $this->session->all_userdata();
        if (isset($sess[$sess_name]) && !empty($sess[$sess_name])) {
            return $sess[$sess_name];
        } else {
            return null;
        }
    }

    public function get_lock_status() {
        $sess = $this->session->all_userdata();
        if (isset($sess[$this->config->session_name . '_lock_screen']) && !empty($sess[$this->config->session_name . '_lock_screen'])) {
            return $sess[$this->config->session_name . '_lock_screen']['status'];
        } else {
            return 0;
        }
    }

    public function auth_user() {
        if ($this->template_configs->_module == 'backend') {
            $_redirect_login = '';
            $txt_flash = 'Welcome to orenoframeworks';
            $sess = $this->_session_auth($this->config->session_name);
            if (isset($sess['is_logged_in']) && !empty($sess['is_logged_in'])) {
                //get permissions
                $permission = $this->get_permission(0);
                if ($permission == false) {
                    $txt_flash = 'You didnt have access group to open this page';
                    $_redirect_login = 'dashboard';
                }
                $lock = $this->get_lock_status();
                if ($lock == true) {
                    $txt_flash = 'Your screen is locked, because inactivity for long time. please insert your password for accessing website.';
                    $_redirect_login = 'lock-screen';
                    if ($this->template_configs->_action == 'lock_screen' || $this->template_configs->_action == 'un_lock_screen') {
                        $_redirect_login = '';
                    }
                }
                if ($this->template_configs->_action == 'login') {
                    $_redirect_login = 'dashboard';
                }
            } else {
                if ($this->template_configs->_action != 'login') {
                    if ($this->template_configs->_action == 'auth' || $this->template_configs->_action == 'logout') {
                        $_redirect_login = '';
                    } else {
                        $txt_flash = 'Please login first to open this page';
                        $_redirect_login = 'logout';
                    }
                }
            }
            //debug($_redirect_login);
            $this->session->set_flashdata('message', $txt_flash);
            if ($_redirect_login != '') {
                redirect(base_backend_url($_redirect_login));
            }
        }
    }

    public function auth_pesky() {
        if ($this->template_configs->_module == 'pesky') {
            $permission = $this->get_permission();
            $sess = $this->_session_auth($this->config->session_pesky);
            if ($permission == true) {
                $_redirect_login = 'login';
                if ($sess['is_logged_in']) {
                    $_redirect_login = 'dashboard';
                    if ($this->template_configs->_action == 'dashboard') {
                        $_redirect_login = '';
                    }
                }
            } else {
                $_redirect_login = '';
            }
            if ($_redirect_login != '') {
                redirect(base_pesky_url($_redirect_login));
            }
        }
    }

    protected function get_permission($is_public = 0) {
        $this->load->model('Tbl_group_permissions');
        $arr_opt = array(
            'conditions' => array('a.is_public' => $is_public, 'b.module' => $this->template_configs->_module, 'b.class' => $this->template_configs->_class, 'b.action' => $this->template_configs->_action, 'a.is_allowed' => 1, 'a.is_active' => 1),
            'joins' => array(
                array(
                    'table' => 'tbl_permissions b',
                    'conditions' => 'b.id = a.permission_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_groups c',
                    'conditions' => 'c.id = a.group_id',
                    'type' => 'left'
                )
            )
        );
        $res = $this->Tbl_group_permissions->find('first', $arr_opt);
        if (isset($res) && !empty($res)) {
            return true;
        } else {
            return false;
        }
    }

    public function auth_config() {
        $session_name = '';
        if ($this->template_configs->_module == 'backend') {
            $session_name = $this->config->session_name;
        } elseif ($this->template_configs->_module == 'pesky') {
            $session_name = $this->config->session_pesky;
        }
        $sess = $this->_session_auth($session_name);
        if (isset($sess['is_logged_in']) && !empty($sess['is_logged_in'])) {
            $arr = '';
            foreach ($sess AS $key => $val) {
                if (!empty($arr))
                    $arr .= ' ';
                $arr .= 'var ' . $key . ' = "' . $val . '";';
            }
            $this->load->vars('_load_auth_config_var', $sess);
            $this->load->vars('_load_auth_config_ajax_var', $arr);
            $this->auth_config = new stdClass();
            if (isset($sess) && !empty($sess)) {
                foreach ($sess AS $key => $val) {
                    $this->auth_config->{$key} = $val;
                }
            }
        }
    }

    public function fnReshapePath($path = null, $rm = '') {
        if ($path != null) {
            $first = str_replace('..', '', $path);
            $second = explode('/', $first);
            $return = array();
            if ($second) {
                foreach ($second AS $key => $val) {
                    if (is_array($rm) == true && $val != '' && $rm[0] == 'replace' && $val != 'modules') {
                        if ($val == $rm[1]) {
                            $val = $rm[2];
                        }
                        $return[] = $val;
                    } elseif ($val != '' && $val != $rm && $val != 'modules') {
                        $return[] = $val;
                    }
                }
            }
            return implode('/', $return);
        }
    }

    public function lang() {
        $this->default_lang();
        $this->load_lang();
    }

    protected function default_lang($lang = 'english') {
        $res = $this->session->userdata('_lang');
        if (!isset($res) || empty($res)) {
            $this->session->set_userdata('_lang', $lang);
            return true;
        }
    }

    protected function load_lang() {
        $lang = $this->session->userdata('_lang');
        if (!isset($lang) || empty($lang)) {
            $this->session->set_userdata('_lang', $lang);
            $this->lang->load('admin', $lang);
        } else {
            $this->lang->load('admin', $lang);
        }
        $this->lang->load('global', $lang);
    }

    public function is_devices() {
        $this->load->library(array('mobile_detect'));
        if ($this->mobile_detect->isMobile() == true) {
            $devices = 'mobile';
        } elseif ($this->mobile_detect->isTablet() == true) {
            $devices = 'tablet';
        } else {
            $devices = 'desktop';
        }
        return $devices;
    }

    public function _get_notif() {
        $this->load->model('Tbl_component_notifications');
        $res = $this->Tbl_component_notifications->find('all', array(
            'fields' => array('a.*', 'b.name as category_name', 'c.name status_name'),
            'conditions' => array('a.status_id' => 1, 'a.is_active' => 1, 'a.create_date >' => date('Y-m-d H:i:s', strtotime('-1day'))),
            'joins' => array(
                array(
                    'table' => 'tbl_component_notification_categories b',
                    'conditions' => 'b.id = a.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_component_notification_status c',
                    'conditions' => 'c.id = a.status_id',
                    'type' => 'left'
                )
            )
                )
        );
        $total = 0;
        $arr = '';
        if (isset($res) && !empty($res)) {
            $total = count($res);
            foreach ($res AS $key => $val) {
                $time = date_diff(date_create($val['create_date']), date_create());
                $new_time = '';
                if ($time->h < 1 && $time->i == 0)
                    $new_time = 'just now';
                if ($time->h < 1 && $time->i > 0)
                    $new_time = $time->i . ' mins';
                if ($time->h > 1)
                    $new_time = $time->h . 's ago';
                $title_1 = str_replace('/', '-', trim($val['title']));
                $title_2 = str_replace('--', '-', $title_1);
                $title = strtolower($title_2);
                $arr .= '
                <li>
                    <a href="' . base_backend_url('profiles/notification/detail/' . $title . '/' . $val['id']) . '">
                        <span class="time">' . $new_time . '</span>
                        <span class="details">
                            <span class="label label-sm label-icon ' . $val['label'] . '">
                                <i class="fa fa-plus"></i>
                            </span> ' . $val['title'] . '
                        </span>
                    </a>
                </li>';
            }
        }
        $result = array(
            'total' => $total,
            'data' => $arr
        );
        $this->load->vars('_load_notif', $result);
    }

    public function _get_msg() {
        $this->load->model('Tbl_component_messages');
        $res = $this->Tbl_component_messages->find('all', array(
            'fields' => array('a.*', 'b.name as category_name', 'c.name status_name', 'd.username user_name', 'e.img user_img'),
            'conditions' => array('a.status_id' => 1, 'a.is_active' => 1, 'a.create_date >' => date('Y-m-d H:i:s', strtotime('-1day'))),
            'joins' => array(
                array(
                    'table' => 'tbl_component_message_categories b',
                    'conditions' => 'b.id = a.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_component_message_status c',
                    'conditions' => 'c.id = a.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_users d',
                    'conditions' => 'd.id = a.from_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_user_profiles e',
                    'conditions' => 'e.id = d.id',
                    'type' => 'left'
                )
            )
                )
        );
        $total = 0;
        $arr = '';
        if (isset($res) && !empty($res)) {
            $total = count($res);
            foreach ($res AS $key => $val) {
                $time = date_diff(date_create($val['create_date']), date_create());
                $new_time = '';
                if ($time->h < 1 && $time->i == 0)
                    $new_time = 'just now';
                if ($time->h < 1 && $time->i > 0)
                    $new_time = $time->i . ' mins';
                if ($time->h > 1)
                    $new_time = $time->h . 's ago';
                $title_1 = str_replace('/', '-', trim($val['subject']));
                $title_2 = str_replace('--', '-', $title_1);
                $title = strtolower($title_2);
                $img = $val['user_img'];
                $arr .= '
                <li>
                    <a href="' . base_backend_url('profiles/message/detail/' . $title . '/' . $val['id']) . '">
                        <span class="photo">
                                <img src="' . static_url($img) . '" class="img-circle" alt="User Images">
                        </span>
                        <span class="subject">
                                <span class="from"> ' . $val['user_name'] . ' </span>
                                <span class="time">' . $new_time . '</span>
                        </span>
                        <span class="message"> ' . $title . ' </span>
                    </a>
                </li>';
            }
        }
        $result = array(
            'total' => $total,
            'data' => $arr
        );
        $this->load->vars('_load_msg', $result);
    }

    public function _get_task() {
        $this->load->model('Tbl_component_tasks');
        $res = $this->Tbl_component_tasks->find('all', array(
            'fields' => array('a.*', 'b.name as category_name', 'c.name status_name'),
            'conditions' => array('a.status_id' => 1, 'a.is_active' => 1, 'a.create_date >' => date('Y-m-d H:i:s', strtotime('-1day'))),
            'joins' => array(
                array(
                    'table' => 'tbl_component_task_categories b',
                    'conditions' => 'b.id = a.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_component_task_status c',
                    'conditions' => 'c.id = a.status_id',
                    'type' => 'left'
                )
            )
                )
        );
        $total = 0;
        $arr = '';
        if (isset($res) && !empty($res)) {
            $total = count($res);
            foreach ($res AS $key => $val) {
                $time = date_diff(date_create($val['create_date']), date_create());
                $new_time = '';
                if ($time->h < 1 && $time->i == 0)
                    $new_time = 'just now';
                if ($time->h < 1 && $time->i > 0)
                    $new_time = $time->i . ' mins';
                if ($time->h > 1)
                    $new_time = $time->h . 's ago';
                $title_1 = str_replace('/', '-', trim($val['title']));
                $title_2 = str_replace('--', '-', $title_1);
                $title = strtolower($title_2);
                $arr .= '
                    <li>
                        <a href="' . base_backend_url('profiles/task/detail/' . $title . '/' . $val['id']) . '">
                            <span class="task">
                                <span class="desc">' . $title . '</span>
                                <span class="percent">' . $val['progress'] . '%</span>
                            </span>
                            <span class="progress">
                                <span style="width: ' . $val['progress'] . '%;" class="progress-bar progress-bar-success" aria-valuenow="' . $val['progress'] . '0" aria-valuemin="0" aria-valuemax="100">
                                        <span class="sr-only">' . $val['progress'] . '% Complete</span>
                                </span>
                            </span>
                        </a>
                    </li>';
            }
        }
        $result = array(
            'total' => $total,
            'data' => $arr
        );
        $this->load->vars('_load_task', $result);
    }

    public function get_footer() {
        $this->load->model(array('Tbl_cms_contents'));
        $result = $this->Tbl_cms_contents->find('all', array('conditions' => array('is_footer' => 1, 'is_active' => 1)));
        $arr = array();
        if ($result) {
            foreach ($result AS $key => $val) {
                $arr{$val['title']} = $val['text'];
            }
        }
        $this->load->vars('_load_footer', $arr);
    }

    public function get_page($uri = 'home') {
        $this->load->model('Tbl_cms_contents');
        $message = '';
        $res = $this->Tbl_cms_contents->find('first', array(
            'fields' => array('a.*', 'b.content_category_id', 'c.name', 'd.name menu_name', 'd.path menu_path'),
            'conditions' => array('a.is_page' => 1, 'a.is_active' => 1, 'd.module_id' => (int) $this->get_module_id(), 'd.path' => $uri),
            'joins' => array(
                array(
                    'table' => 'tbl_cms_category_contents b',
                    'conditions' => 'b.content_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_cms_categories c',
                    'conditions' => 'c.id = b.content_category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_menus d',
                    'conditions' => 'd.id = a.menu_id',
                    'type' => 'left'
                )
            )
        ));
        $data['uri'] = $uri;
        $data['content'] = array();
        if (isset($res) && !empty($res)) {
            $data['content'] = $res;
        } else {
            if ($uri == '#login' || $uri == '#dashboard') {
                $sess = $this->_session_auth($this->config->session_pesky);
                $data['dynamic_ajax'] = 1;
                $data['uri'] = str_replace('#', '', $uri);
                if ($sess == null) {
                    $data['dynamic_ajax'] = 1;
                    $data['uri'] = 'login';
                    if ($uri != '#login') {
                        $data['message'] = 'You cannot accessing ' . $uri . ', please login first for accessing this page!!!';
                    }
                }
            } elseif ($uri == '#logout') {
                $this->oreno_auth->destroy_session($this->_session_auth($this->config->session_pesky));
                $data['dynamic_ajax'] = 1;
                $data['uri'] = 'login';
                if ($uri != '#login') {
                    $message = 'Successfully logout from system';
                }
                $data['_load_ajax_var'] = array('_redirect' => true, '_uri' => '#login', 'message' => $message);
            }
        }
        return $data;
    }

}
