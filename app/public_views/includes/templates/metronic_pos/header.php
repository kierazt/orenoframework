<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo global_uri('dashboard'); ?>">
                <img src="<?php echo static_url('templates/metronics/assets/layouts/layout/img/logo.png') ?>" alt="logo" class="logo-default" /> 
				<p style="color:#fff">P.O.S</p>
			</a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default"> <?php echo isset($_load_notif) ? $_load_notif['total'] : 0; ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <span class="bold"><?php echo isset($_load_notif) ? $_load_notif['total'] : 0; ?> pending</span> notifications</h3>
                            <a href="<?php echo base_backend_url('profiles/notification/view/'); ?>">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                <?php echo isset($_load_notif['data']) ? $_load_notif['data'] : ''; ?>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN INBOX DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span class="badge badge-default"> <?php echo isset($_load_msg) ? $_load_msg['total'] : 0; ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>You have
                                <span class="bold"><?php echo isset($_load_msg) ? $_load_msg['total'] : 0; ?> New</span> Messages
                            </h3>
                            <a href="<?php echo base_backend_url('profiles/messages/view/'); ?>">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <?php echo isset($_load_msg['data']) ? $_load_msg['data'] : ''; ?>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END INBOX DROPDOWN -->
                <!-- BEGIN TODO DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-calendar"></i>
                        <span class="badge badge-default"> <?php echo isset($_load_task) ? $_load_task['total'] : 0; ?> </span>
                    </a>
                    <ul class="dropdown-menu extended tasks">
                        <li class="external">
                            <h3>You have
                                <span class="bold"><?php echo isset($_load_task) ? $_load_task['total'] : 0; ?> pending</span> tasks</h3>
                            <a href="<?php echo base_backend_url('profiles/task/view/'); ?>">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <?php echo isset($_load_task['data']) ? $_load_task['data'] : ''; ?>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END TODO DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?php echo isset($_load_auth_config_var['img']) ? static_url($_load_auth_config_var['img']) : static_url('templates/metronics/assets/layouts/layout/img/avatar3_small.jpg');  ?>" />
                        <span class="username username-hide-on-mobile"> <?php echo isset($_load_auth_config_var['email']) ? $_load_auth_config_var['email'] : '';  ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="<?php echo global_uri($_var_template->_module, 'my-profile'); ?>">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li>
                            <a href="<?php echo global_uri($_var_template->_module, 'my-inbox'); ?>">
                                <i class="icon-envelope-open"></i> My Inbox
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo global_uri($_var_template->_module, 'my-task'); ?>">
                                <i class="icon-rocket"></i> My Tasks
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="<?php echo global_uri($_var_template->_module, 'lock-screen'); ?>">
                                <i class="icon-lock"></i> Lock Screen </a>
                        </li>
                        <li>
                            <a href="<?php echo global_uri($_var_template->_module, 'logout'); ?>">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
