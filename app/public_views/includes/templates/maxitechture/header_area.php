<!--================Header Menu Area =================-->
<style>
    .nav > li > a:focus, .nav > li > a:hover{
        text-decoration: none;
        background-color: #ccc;
        opacity:0.5;
    }
</style>
<header class="header_area">
    <div class="main_menu" id="mainNav">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <span style="width:100%; position:static; z-index:999; float:left;">
                    <?php if ($_var_template->_module == 'frontend') : ?>
                        <a class="navbar-brand logo_h" href="<?php echo base_url(); ?>" style="width:250px;">
                            <img src="<?php //echo static_url('images/indosporttiming-logo.png');   ?>" alt="NetofCreatives.com" style="width:80%;">
                        </a>
                    <?php else: ?>
                        <a class="navbar-brand logo_h" href="<?php echo base_url(); ?>" style="width:250px">
                            <img src="<?php echo static_url('images/indosporttiming-logo.png'); ?>" alt="NetofCreatives.com" style="width:80%;">
                        </a>
                    <?php endif; ?>
                <span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto" style="float:right">
                        <?php if (isset($_menu['nodes']) && !empty($_menu['nodes'])): ?>
                            <?php foreach ($_menu['nodes'] AS $key => $values): ?>
                                <li class="nav-item"><a class="nav-link" data-id="<?php echo $values['root_id']; ?>" href="<?php echo base_pesky_url($values['root_path']); ?>"><?php echo $values['text']; ?></a></li>
                                <?php if (isset($values['nodes']) && !empty($values['nodes'])): ?>
                                    <?php foreach ($values['nodes'] AS $k => $val): ?>
                                        <li class="nav-item submenu dropdown">
                                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $val['text']; ?></a>
                                            <ul class="dropdown-menu">
                                                <?php if (isset($val['nodes']) && !empty($val['nodes'])): ?>
                                                    <?php foreach ($val['nodes'] AS $j => $v): ?>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#"><?php echo $v['text']; ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
