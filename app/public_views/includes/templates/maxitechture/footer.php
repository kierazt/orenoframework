<!--================Footer Area =================-->
<footer class="footer_area p_120">
    <div class="container">
        <div class="row footer_inner">
            <div class="col-lg-5 col-sm-6"><?php echo isset($_load_footer['About']) ? $_load_footer['About'] : ''; ?></div>
            <div class="col-lg-5 col-sm-6"><?php echo isset($_load_footer['Newsletter']) ? $_load_footer['Newsletter'] : ''; ?></div>
            <div class="col-lg-2"><?php echo isset($_load_footer['Socials']) ? $_load_footer['Socials'] : ''; ?></div>
        </div>
    </div>
</footer>
<!--================End Footer Area =================-->