<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="<?php echo static_url('templates/maxitechture/js/jquery-3.2.1.min.js');?>"></script>
<script src="<?php echo static_url('lib/packages/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/js/popper.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/js/bootstrap.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/js/stellar.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/lightbox/simpleLightbox.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/nice-select/js/jquery.nice-select.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/isotope/imagesloaded.pkgd.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/isotope/isotope-min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/owl-carousel/owl.carousel.min.js');?>"></script>

<script src="<?php echo static_url('templates/maxitechture/js/jquery.ajaxchimp.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/counter-up/jquery.waypoints.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/counter-up/jquery.counterup.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/js/mail-script.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/vendors/popup/jquery.magnific-popup.min.js');?>"></script>
<script src="<?php echo static_url('templates/maxitechture/js/theme.js');?>"></script>

<?php echo isset($_ajax_var_configs) ? '<script>' . $_ajax_var_configs . '</script>' : ''; ?>
<?php echo isset($_ajax_var_template) ? '<script>' . $_ajax_var_template . '</script>' : ''; ?>
<?php echo isset($_load_ajax_var) ? '<script>' . $_load_ajax_var . '</script>' : ''; ?>
<?php echo isset($_js_function) ? '<script>' . $_js_function . '</script>' : ''; ?>
<?php isset($_var_template->_app_js) ? $this->load->view($_var_template->_app_js) : ''; ?>

<script src="<?php //echo static_url('templates/maxitechture/js/theme.js');?>"></script>
<script src="<?php echo static_url('lib/single/base64.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-toastr/toastr.min.js') ?>" type="text/javascript"></script>
<?php echo isset($_load_js) ? $_load_js : ''; ?>
<!-- END PAGE LEVEL PLUGINS -->

<?php if($_var_template->_global_js) $this->load->view($_var_template->_global_js) ?>
<?php if($_var_template->_view_js) $this->load->view($_var_template->_view_js) ?>