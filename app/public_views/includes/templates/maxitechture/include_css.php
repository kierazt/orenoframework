<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/css/bootstrap.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/linericon/style.cs') ?>s">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/owl-carousel/owl.carousel.min.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/lightbox/simpleLightbox.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/nice-select/css/nice-select.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/animate-css/animate.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/vendors/popup/magnific-popup.css') ?>">
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo static_url('lib/packages/jquery-ui/themes/eggplant/jquery-ui.min.css') ?>">
<!-- main css -->
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/css/style.css') ?>">
<link rel="stylesheet" href="<?php echo static_url('templates/maxitechture/css/responsive.css') ?>">
<?php echo isset($_load_css) ? $_load_css : ''; ?>
<style>
    h3{
        color:#000
    }
    #participate{
        padding:2px;
        color:#fff;
        background-color:#8ec460;
        opacity:0.8;
        border:1px solid;
        border-radius:5px;
    }
    .dataTables_filter{
        text-align:right;
        padding-right:20px;
    }
    .pagination{
        margin-top:6px; 
        margin-left:5px;
        margin-bottom:2px;
        padding-top:4px;
    }
    .pagination .digit{
        padding: 0 10px 0 10px; 
        display: inline;
    }
    .pagination .digit:hover, .pagination .current{
        padding: 2px 10px 2px 10px; 
        background: #CCC; 
        font-weight: 900; 
        -moz-border-radius:5px
    }
</style>