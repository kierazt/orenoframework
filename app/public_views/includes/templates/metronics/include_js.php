<script src="<?php echo static_url('lib/packages/jquery/jquery-3.3.1.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('lib/packages/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap/js/bootstrap.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<?php echo isset($_ajax_var_configs) ? '<script>' . $_ajax_var_configs . '</script>' : ''; ?>
<?php echo isset($_ajax_var_template) ? '<script>' . $_ajax_var_template . '</script>' : ''; ?>
<?php echo isset($_load_ajax_var) ? '<script>' . $_load_ajax_var . '</script>' : ''; ?>
<?php echo isset($_js_function) ? '<script>' . $_js_function . '</script>' : ''; ?>
<?php echo isset($_load_auth_config_ajax_var) ? '<script>' . $_load_auth_config_ajax_var . '</script>' : ''; ?>
<?php isset($_var_template->_app_js) ? $this->load->view($_var_template->_app_js) : ''; ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo static_url('lib/single/base64.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/layouts/layout/scripts/layout.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-daterangepicker/moment.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/pages/scripts/ui-blockui.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/layouts/global/scripts/quick-sidebar.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootbox/bootbox.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-toastr/toastr.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootbox/bootbox.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/bootbox/bootbox.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/pages/scripts/components-bootstrap-switch.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo static_url('templates/metronics/assets/global/plugins/icheck/icheck.min.js') ?>" type="text/javascript"></script>
<?php echo isset($_load_js) ? $_load_js : ''; ?>
<!-- END PAGE LEVEL PLUGINS -->

<?php isset($_var_template->_global_js) ? $this->load->view($_var_template->_global_js) : ''; ?>
<?php isset($_var_template->_view_js) ? $this->load->view($_var_template->_view_js) : ''; ?>
