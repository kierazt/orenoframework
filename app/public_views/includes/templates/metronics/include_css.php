<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/fullcalendar/fullcalendar.min.css') ?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo static_url('templates/metronics/assets/global/plugins/icheck/skins/all.css') ?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo static_url('templates/metronics/assets/global/css/components-rounded.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/global/css/plugins.min.css') ?>" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo static_url('templates/metronics/assets/layouts/layout/css/layout.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo static_url('templates/metronics/assets/layouts/layout/css/themes/darkblue.min.css') ?>" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo static_url('templates/metronics/assets/layouts/layout/css/custom.min.css') ?>" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<?php echo isset($_load_css) ? $_load_css : ''; ?>
<style>
    .dataTables_filter{
        float:right;
    }
</style>