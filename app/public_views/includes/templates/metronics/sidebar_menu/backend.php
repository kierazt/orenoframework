<?php if ($_var_template->_module == 'backend'): ?>
    <?php if (isset($_menu) && !empty($_menu)): ?>
        <li class="heading">
            <h3 class="uppercase"><?php echo $_menu['text']; ?></h3>
        </li>
        <?php if (isset($_menu['nodes']) && !empty($_menu['nodes'])): ?>
            <?php foreach ($_menu['nodes'] AS $key => $values): ?>
                <li class="nav-item ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa <?php echo $values['root_icon']; ?>"></i>
                        <span class="title"><?php echo $values['text']; ?></span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if (isset($values['nodes']) && !empty($values['nodes'])): ?>
                            <?php foreach ($values['nodes'] AS $k => $val): ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo $path = ($val['root_path'] == '#' || $val['root_path'] == '') ? 'javascript:;' : base_developer_url($val['root_path']); ?>" class="nav-link nav-toggle">
                                        <i class="fa <?php echo $val['root_icon']; ?>"></i>
                                        <span class="title"><?php echo $val['text']; ?></span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <?php if (isset($val['nodes']) && !empty($val['nodes'])): ?>
                                            <?php foreach ($val['nodes'] AS $k => $v): ?>
                                                <li class="nav-item  ">
                                                    <a href="<?php echo $path = ($v['root_path'] == '#' || $v['root_path'] == '') ? 'javascript:;' : base_developer_url($v['root_path']); ?>" class="nav-link ">
                                                        <i class="fa<?php echo $v['root_icon']; ?>"></i>
                                                        <span class="title"><?php echo $v['text']; ?></span>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endforeach; ?>		
        <?php endif; ?>
    <?php endif; ?>	
<?php endif; ?>