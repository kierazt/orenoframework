<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['title_for_layout'] = 'welcome';
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function get_data($id = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_menus');
            $res = $this->Tbl_menus->find('first', array(
                'fields' => array('a.*', 'b.content_category_id', 'c.title'),
                'conditions' => array('menu_id' => $id),
                'joins' => array(
                    array(
                        'table' => 'tbl_cms_category_contents b',
                        'conditions' => 'b.content_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_cms_categories c',
                        'conditions' => 'c.id = b.content_category_id',
                        'type' => 'left'
                    )
                )
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function get_page() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_cms_contents');
            $res = $this->Tbl_cms_contents->find('first', array(
                'fields' => array('a.*', 'b.content_category_id', 'c.name'),
                'conditions' => array('a.is_page' => 1, 'a.is_active' => 1, 'd.path' => $post['uri']),
                'joins' => array(
                    array(
                        'table' => 'tbl_cms_category_contents b',
                        'conditions' => 'b.content_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_cms_categories c',
                        'conditions' => 'c.id = b.content_category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_menus d',
                        'conditions' => 'd.id = a.menu_id',
                        'type' => 'left'
                    )
                )
            ));

            $data['uri'] = $post['uri'];
            $data['content'] = array();
            $data['dynamic_ajax'] = 0;

            if (isset($res) && !empty($res)) {
                $data['content'] = $res;
            } else {
                if ($post['uri'] == '#login') {
                    $data['dynamic_ajax'] = 1;
                    $data['uri'] = str_replace('#', '', $post['uri']);
                }
            }
            $this->parser->parse('layouts/pages/empty.phtml', $data);
        }
    }

}
