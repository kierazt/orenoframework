<script>
    var current_url = document.location.href;
    var current_page = function () {
        var uri_sp = current_url.split('/');
        if (uri_sp[3]) {
            return uri_sp[3];
        }
    }
    var Ajax = function () {
        return {
            init: function () {
                toastr.success(mod_active + ' is ready');
                $('.note-fontname').on('click', function () {
                    alert('wew');
                });
                if (current_page()) {
                    var formdata = {
                        uri: current_page()
                    };
                    $.ajax({
                        url: global_uri('settings/user/get_page/'),
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            $('#result_').html(response);
                        },
                        error: function (response) {
                            $('#result_').html(response);
                        }
                    });
                } else {
                    var formdata = {
                        uri: '#home'
                    };
                    $.ajax({
                        url: global_uri('settings/user/get_page/'),
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            if (response != 'null') {
                                $('#result_').html(response);
                            } else {
                                $('#result_').html(formdata.uri);
                            }
                        },
                        error: function (response) {
                            $('#result_').html(response);
                        }
                    });
                }

                $('.login-form').on('submit', function () {
                    alert('stop');
                    return false;
                });

                $('.nav-link').on('click', function () {
                    display_load();

                    var uri_ = $(this).attr('href');
                    var formdata = {
                        uri: uri_
                    };
                    setTimeout(function () {

                        $.ajax({
                            url: global_uri('settings/user/get_page/'),
                            method: "POST", //First change type to method here
                            data: formdata,
                            success: function (response) {
                                if (response != 'null') {
                                    $('#result_').html(response);
                                } else {
                                    $('#result_').html(uri_);
                                }
                                hide_load();
                            },
                            error: function (response) {
                                $('#result_').html(response);
                                hide_load();
                            }
                        });
                    }, 900);
                });

            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>