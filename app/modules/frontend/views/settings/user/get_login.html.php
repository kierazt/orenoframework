<div style="width:33.33%">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form">
        <div class="form-title">
            <span class="form-title" style="font-size:24px"><b>Welcome, Please login first.</b></span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
        <div class="form-actions">
            <table style="width:100%">
                <tr>
                    <td style="width:50%; text-align:left">
                        <button type="submit" class="btn red btn-block uppercase" style="width:50%">Login</button>
                    </td>
                    <td style="width:50%; text-align:right">
                        <input type="checkbox" name="remember" value="1" />Remember me </label>					
                    </td>
                </tr>
            </table>
        </div>
        <div class="form-actions">
            <div class="pull-right forget-password-block">
                <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
            </div>
        </div>

        <div class="create-account">
            <p>
                <a href="javascript:;" class="btn-primary btn" id="register-btn">Create an account</a>
            </p>
        </div>
    </form>
</div>