<div style="margin: 0 auto; min-height:100vh"> 
    <div style="padding:20px; background-color:#fff; opacity:0.7; height:80%">
        <?php
        if (isset($content['text']) && !empty($content['text'])) {
            echo html_entity_decode($content['text']);
        } else {
            if (isset($uri) && !empty($uri)) {
                if ($dynamic_ajax == 1) {
                    $load_html = $_var_template->_directory . '/' . $_var_template->_class . '/get_' . $uri . '.html.php';
                    $load_js = $_var_template->_directory . '/' . $_var_template->_class . '/get_' . $uri . '.js.php';
                    $this->load->view($load_html);
                    $this->load->view($load_js);
                } else {
                    echo '<center><h2>page content cannot found at {uri}!!!</h2><br/><small>error2</small></center>';
                }
            } else {
                echo '<center><h2>cannot found {uri} in web system!!!</h2><br/><small>error1</small>dynamic_ajax</center>';
            }
        }
        ?>
    </div>
</div>