<div class="inbox">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="inbox-sidebar">
                    <a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block">
                        <i class="fa fa-edit"></i> Compose 
                    </a>
                    <ul class="inbox-nav">
                        <li class="active">
                            <a href="javascript:;" data-type="inbox" data-title="Inbox"> Inbox
                                <span class="badge badge-success">3</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="important" data-title="Inbox"> Important </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="sent" data-title="Sent"> Sent </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="draft" data-title="Draft"> Draft
                                <span class="badge badge-danger">8</span>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:;" class="sbold uppercase" data-title="Trash"> Trash
                                <span class="badge badge-info">23</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="inbox" data-title="Promotions"> Promotions
                                <span class="badge badge-warning">2</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="inbox" data-title="News"> News </a>
                        </li>
                    </ul>                
                </div>
            </div>
            <div class="col-md-10">
                <div class="inbox-body"></div>
            </div>
        </div>
    </div>
</div>