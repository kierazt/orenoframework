<script>
    var current_url = document.location.href;
    var current_page = function () {
        var uri_sp = current_url.split('/');
        if (uri_sp[3]) {
            return uri_sp[3];
        }
    };
    var fnListMessage = function () {
        var formdata = {
            uri: 'get_all_message'
        };
        $.ajax({
            url: base_backend_url + 'profiles/message/get_page',
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                $('.inbox-body').html(response);
            },
            error: function (response) {
                $('.inbox-body').html(response);
            }
        });
    };
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('message js ready!!!');
                fnListMessage();
                var table = $('#datatable_ajax_all').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'profiles/message/fn_get_all_message/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "sender"},
                        {"data": "message"},
                        {"data": "attachment"}
                    ],
                    "drawCallback": function (settings) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });
</script>