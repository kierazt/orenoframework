<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>

            </div>
            <div class="portlet-body">
                <form method="POST" id="add_edit">
                    <div class="modal-header">
                        <button type="button" class="close" data-action="close-modal" id="back_to_prev" aria-hidden="true"></button>
                        <h4 class="modal-title" id="title_mdl"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Title</label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                <input class="form-control" type="text" name="title" /> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <div id="summernote"></div>
                        </div>
                        <div class="form-group">
                            <label>Menu</label>
                            <div class="input-group input-group-sm">
                                <select class="form-control set_menu" name="menu" id="mmenu">
                                    <option>-- select one --</option>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn green get_menu" type="button">request!</button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </div>								
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="description"></textarea>
                        </div>
                        <div class="form-group" style="height:30px">
                            <label>Active</label><br/>
                            <input type="checkbox" class="make-switch" data-size="small" name="status"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="back_to_prev" class="btn dark btn-outline">Close</button>
                            <button type="submit" class="btn green">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>