<script>
    var TableDatatablesAjax = function () {

        return {
            //main function to initiate the module
            init: function () {
                $('#summernote').summernote({
                    placeholder: 'text'
                });
                $('#back_to_prev').on('click', function () {
                    App.startPageLoading({animate: true});
                    setTimeout(function () {
                        App.stopPageLoading();
                        window.location = base_developer_url('app/content/view');
                    }, 2000);
                });
                $('.get_menu').on('click', function () {
                    $.ajax({
                        url: base_developer_url('app/content/get_menu/'),
                        method: "POST", //First change type to method here
                        success: function (response) {
                            $('.set_menu').html(response);
                        },
                        error: function (response) {
                            toastr.error('Failed ' + response);
                        }

                    });
                });
                $("#add_edit").submit(function () {
                    var title = $('input[name="title"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var menu = $('#mmenu').val();
                    var content = $('#summernote').summernote('code');
                    var uri = base_developer_url('app/content/insert/');
                    var txt = 'add new content';
                    var formdata = {
                        title: title,
                        content: content,
                        menu: menu,
                        description: $('textarea[name="description"]').val(),
                        active: is_active
                    };
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            close_modal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            close_modal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>