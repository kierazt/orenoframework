<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>

            </div>
            <div class="portlet-body">
                <form method="POST" id="add_edit">
                    <div class="modal-header">
                        <button type="button" class="close" data-action="close-modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="title_mdl"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Content</label>
                            <div class="cust_toolbar">
                                <div id="add-header" class="btn green">add header</div>
                            </div>
                            <div id="summernote"><?php echo $content['text']; ?></div>
                        </div>
                        <div class="modal-footer">
                            <input type="text" name="id" value="<?php echo $content['id']; ?>" hidden />
                            <button type="button" id="back_to_prev" class="btn dark btn-outline">Close</button>
                            <button type="submit" class="btn green">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>