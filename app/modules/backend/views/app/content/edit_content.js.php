<script>
    var TableDatatablesAjax = function () {

        return {
            //main function to initiate the module
            init: function () {
                $('#summernote').summernote({
                    placeholder: 'text'
                });
                $('#back_to_prev').on('click', function () {
                    App.startPageLoading({animate: true});
                    setTimeout(function () {
                        App.stopPageLoading();
                        window.location = base_developer_url('app/content/view');
                    }, 2000);
                });
                $('#add-header').on('click', function () {
                    var str = '<h2></h2>';
                    console.log(str);
                    $('#summernote').summernote('insertText', str);
                });
                $("#add_edit").submit(function () {
                    App.startPageLoading({animate: true});
                    var id = $('input[name="id"]').val();
                    var content = $('#summernote').summernote('code');
                    var txt = 'add new content';
                    var formdata = {id: Base64.encode(id), content: content};
                    var uri = base_developer_url('app/content/update_content/');
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            close_modal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            close_modal();
                        }

                    });
                    App.stopPageLoading();
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>