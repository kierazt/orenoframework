<script>
    var fnInitTree = function (id, key, name, key2) {
        $.ajax({
            async: true,
            type: "GET",
            url: base_backend_url + ('settings/menu/get_menu/' + id + '/' + key + '/' + name),
            dataType: "json",
            success: function (json) {
                if (key2) {
                    var el = '#tree' + key2 + id;
                } else {
                    el = '#tree' + id;
                }
                $(el).jstree({
                    "core": {
                        "data": json,
                        "check_callback": function (operation, node, node_parent, node_position, more) {
                            console.log(operation);
                            console.log(node);
                            //console.log(node_parent);
                            //console.log(node_position);
                            //console.log(more);
                            if (operation == 'create_node') {
                                // console.log(node);
                                //console.log(node_parent);
                            } else if (operation == 'rename_node') {
                                console.log(node);
                                var arrAddMenu = {
                                    parent_id: id,
                                    value: node.text
                                };
                                $.ajax({
                                    url: base_backend_url + ('settings/menu/add_menu/1'),
                                    method: "POST", //First change type to method here
                                    data: arrAddMenu,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                    }

                                });
                            } else if (operation == 'delete_node') {

                            }
                        },
                        "themes": {
                            "responsive": false
                        }
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-arrow-circle-o-down icon-lg"
                        },
                        "file": {
                            "icon": "fa fa-arrow-circle-o-right icon-lg"
                        }
                    },
                    "state": {"key": "demo2"},
                    "plugins": ["contextmenu", "dnd", "state", "types", "html_data", "contextmenu"],
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    };


    var Index = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('menu js is ready');
                //fnGetModule();
                fnInitTree(1, 0, 'frontend');
                fnInitTree(1, 1, 'frontend', '_2_');
                $('.vakata-context a').on('click', function () {
                    alert('WEW');
                });
                $('a').on('click', function () {
                    var href = $(this).attr('href');
                    var id = $(this).attr('data-module_id');
                    var loggedin = $(this).attr('data-loggedin');
                    var module_name = $(this).attr('data-module_name');
                    if (loggedin == 1) {
                        fnInitTree(id, loggedin, module_name, '_2_');
                    } else {
                        fnInitTree(id, loggedin, module_name);
                    }
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Index.init();
    });
</script>