<script>
	var handleSummernote = function () {
        $('#value').summernote({height: 300});
        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    }
	var handleDestroySummernote = function () {
        //$('#value').summernote({height: 300});
        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        $('#value').destroy(); // destroy
    }
    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
				fnToStr('email layout is ready', 'success', 1100);
				$('.btn').on('click', function (e) {
                    e.preventDefault();
                    var value = $(this).attr('data-value');
                    if (value == "add" || value == "edit") {
						handleSummernote();
                    } else if(value == "close"){
						handleDestroySummernote();
					}
                });
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'settings/email_layout/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "keyword"},
                        {"data": "value"},
                        {"data": "active"},
                        {"data": "description"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    console.log(state); // true | false
                    var id = $(this).attr('data-id');
                    var formdata = {
                        active: state
                    };
                    $.ajax({
                        url: base_backend_url + 'settings/email_layout/update_status/' + Base64.encode(id),
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + response);
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });

                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_backend_url + 'settings/email_layout/get_data/' + id, function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            $('input[name="id"]').val(row.id);
                            $('input[name="keyword"]').val(row.keyword);
                            $('input[name="value"]').val(row.value);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $('textarea[name="description"]').val(row.description);
                        });
                    }
                });

                $("#opt_delete").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'settings/email_layout/delete/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
										fnToStr('Successfully ' + response, 'success', 1100);
                                        fnCloseBootbox();
                                    },
                                    error: function () {
										fnToStr('Failed ' + response, 'success', 1100);
                                        fnCloseBootbox();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling delete data ');
                                close_bootbox();
                                close_modal();
                                return false;
                            }
                        });

                    } else {
                        toastr.success('Something went wrong ');
                        close_bootbox();
                        close_modal();
                        return false;
                    }
                });

                $("#opt_remove").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'settings/email_layout/remove/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        close_bootbox();
                                        close_modal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        close_bootbox();
                                        close_modal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling remove data ');
                                close_bootbox();
                                close_modal();
                                return false;
                            }
                        });
                    } else {
                        toastr.success('Something went wrong ');
                        close_bootbox();
                        close_modal();
                        return false;
                    }
                });
                $("#add_edit").submit(function () {
                    var id = $('input[name="id"]').val();
                    var keyword = $('input[name="keyword"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
					var values = $('#value').summernote('code');
                    var uri = base_backend_url + 'settings/email_layout/insert/';
                    var txt = 'add new group';
                    var formdata = {
                        keyword:keyword,
						values:values,
                        description: $('textarea[name="description"]').val(),
                        active: is_active
                    };
                    if (id)
                    {
                        uri = base_backend_url + 'settings/email_layout/update/';
                        txt = 'update group';
                        formdata = {
                            id: Base64.encode(id),
                            keyword:keyword,
							values:values,
                            description: $('textarea[name="description"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            close_modal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            close_modal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>