<script>
    var el = '#method';

    var fnAjaxBtn_select_all = function () {
        $(el).multiSelect('select_all');
    };

    var fnAjaxBtn_deselect_all = function () {
        $(el).multiSelect('deselect_all');
    };

    var fnAjaxBtn_refresh = function () {
        $(el).multiSelect('refresh');
    };

    var fnMultiSelect = function () {
        $(el).multiSelect({
            selectableHeader: "<div class='btn-group btn-group-xs btn-group-solid'><button type='button' class='btn green' onclick='fnAjaxBtn_select_all()' id='select-all'>select all</button></div>",
            selectionHeader: "<div class='btn-group btn-group-xs btn-group-solid'><button type='button' class='btn green' onclick='fnAjaxBtn_deselect_all()' id='deselect-all'>deselect all</button></div>",
        });
        return false;
    };
	
	 var fnGetModule = function () {
        $.ajax({
            url: base_backend_url + 'master/group_permission/get_module/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('.module').html(response);
            },
            error: function () {
                $('.module').html(response);
            }
        });
        return false;
    };

    var fnGetGroup = function () {
        $.ajax({
            url: base_backend_url + 'master/group_permission/get_group/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('.group').html(response);
            },
            error: function () {
                $('.group').html(response);
            }
        });
        return false;
    };

    var fnGetMethod = function () {
        $.ajax({
            url: base_backend_url + 'master/group_permission/get_method/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('#method').html(response);
                fnMultiSelect();
            },
            error: function (response) {
                $('#method').html(response);
            }

        });
    }

    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                fnToStr('view js ready!!!', 'success', 1100);
				
				$('.method_exist').on('click', function(){
					var value = $(this).val();
					console.log(value);
					if(value == 1){
						$('#frmMltSlctMethod').show();
						$('#frmMethodAddNew').hide();
					}else{
						$('#frmMethodAddNew').show();
						$('#frmMltSlctMethod').hide();
					}
				});
                $('.btn').on('click', function (e) {
                    e.preventDefault();
                    var value = $(this).attr('data-value');
                    if (value == "add" || value == "edit") {
						fnGetModule();
                        fnGetGroup();
                        fnGetMethod();
                    }
                });
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'master/group_permission/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "group_name"},
                        {"data": "class"},
                        {"data": "action"},
                        {"data": "allowed"},
                        {"data": "public"},
                        {"data": "active"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    console.log(state); // true | false
                    var id = $(this).attr('data-id');
                    var formdata = {
                        active: state
                    };
                    $.ajax({
                        url: base_backend_url + 'master/group_permission/update_status/' + Base64.encode(id),
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + response);
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });
                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_backend_url + 'master/group_permission/get_data/' + id, function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            $('input[name="id"]').val(row.id);
                            $('input[name="name"]').val(row.name);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $('textarea[name="description"]').val(row.description);
                        });
                    }
                });

                $("#opt_delete").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'master/group_permission/delete/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        close_bootbox();
                                        close_modal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        close_bootbox();
                                        close_modal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling delete data ');
                                close_bootbox();
                                close_modal();
                                return false;
                            }
                        });

                    } else {
                        toastr.success('Something went wrong ');
                        close_bootbox();
                        close_modal();
                        return false;
                    }
                });

                $("#opt_remove").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'master/group_permission/remove/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        close_bootbox();
                                        close_modal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling remove data ');
                                fnCloseBootbox();
                                fnCloseModal();
                                return false;
                            }
                        });
                    } else {
                        toastr.success('Something went wrong ');
                        fnCloseBootbox();
                        fnCloseModal();
                        return false;
                    }
                });
				
                $("#submit").on('click', function () {
                    var uri = base_backend_url + 'master/group_permission/insert/';
                    var id = $('input[name="id"]').val();
                    var is_active = $('[name="status"]').bootstrapSwitch('state');
                    var is_allowed = $('[name="allowed"]').bootstrapSwitch('state');
                    var is_public = $('[name="ispublic"]').bootstrapSwitch('state');
                    var txt = 'add new group_permission';
					var method = [];
					$.each($(".multi-select option:selected"), function(){   
						method.push($(this).val());
					});
                    var formdata = {
                        group: $('#group').val(),
                        class: $('input[name="class"]').val(),
                        method: method,						
                        is_active: is_active,
                        is_allowed: is_allowed,
                        is_public: is_public,
                        description: $('textarea[name="description"]').val(),
                        active: is_active
                    };
                    if (id){
                        uri = base_backend_url + 'master/group_permission/update/';
                        txt = 'update group_permission';
                        formdata = {
                            id: Base64.encode(id),
                            group: $('#group').val(),
                            class: $('input[name="class"]').val(),
                            method: method,
                            is_active: is_active,
                            is_allowed: is_allowed,
                            is_public: is_public,
                            description: $('textarea[name="description"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            fnCloseModal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            fnCloseModal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>