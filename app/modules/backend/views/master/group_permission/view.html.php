<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <div class="btn btn-transparent blue btn-outline btn-circle btn-sm active" data-value="add" id="opt_add">
                            Add
                        </div>
                        <div class="btn btn-transparent green btn-outline btn-circle btn-sm disabled" data-value="edit" id="opt_edit" disabled="">
                            Edit
                        </div>
                        <div class="btn btn-transparent red btn-outline btn-circle btn-sm disabled" data-value="remove" id="opt_remove" disabled="">
                            Remove
                        </div>
                        <div class="btn btn-transparent red btn-outline btn-circle btn-sm disabled" data-value="delete" id="opt_delete" disabled="">
                            Delete
                        </div>
                        <div class="btn btn-transparent yellow btn-outline btn-circle btn-sm" data-value="refresh" id="opt_refresh">
                            Refresh
                        </div>
                    </div>
                    <div class="btn-group">
                        <a class="btn grey btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs"> Tools </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> Export to Excel </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Export to CSV </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Export to XML </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="javascript:;"> Print Invoices </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%"><input type="checkbox" data-checkbox="icheckbox_minimal-grey" class="group-checkable" name="select_all"/></th>
                                <th width="5%"> # </th>
                                <th width="15%"> Group </th>
                                <th width="15%"> Class </th>
                                <th width="15%"> Method </th>
                                <th width="15%"> Is Allowed </th>
                                <th width="15%"> Is Public </th>
                                <th width="15%"> Status </th>
                            </tr>							
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- /.modal -->

<div class="modal fade in" id="modal_add_edit">
    <div class="modal-dialog modal-lg">
        <form method="POST" id="add_edit">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn-close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Modal Title</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
								<div class="form-group">
									<label>Module</label><br/>
                                    <select class="form-control group" name="module" id="module">
                                        <option>-- select one --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Group</label><br/>
                                    <select class="form-control group" name="group" id="group">
                                        <option>-- select one --</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Class</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="class" /> 
                                    </div>
                                </div>
								<div class="form-group">
									<label class="control-label">Method</label>
									<div class="col-md-12">
										<div class="radio-list">
											<label class="radio-inline">
												<div class="radio" id="uniform-method_exist">
													<span><input type="radio" class="method_exist" name="method_exist" id="method_exist" value="1" ></span>
												</div> Method Exist
											</label>
											<label class="radio-inline">
												<div class="radio" id="uniform-method_new">
													<span class="checked"><input type="radio" class="method_exist" name="method_exist" id="method_new" value="0"></span>
												</div> Add New
											</label>
										</div>
									</div>
								</div>
                                <div class="form-group" hidden id="frmMltSlctMethod">
                                    <label class="control-label">Method</label><br/>
                                    <div class="col-md-12">
                                        <select multiple="multiple" class="multi-select" id="method" name="method[]"></select>
                                    </div>									
                                </div>
								<div class="form-group" hidden id="frmMethodAddNew">
                                    <label class="control-label">Add New Method</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="method[]" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="3" name="description"></textarea>
                                </div>
                                <div class="form-group" style="height:30px;">
                                    <label>Allowed</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="allowed"/>
                                </div><br/>
                                <div class="form-group" style="height:30px;">
                                    <label>Public</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="ispublic"/>
                                </div><br/>
                                <div class="form-group" style="height:30px">
                                    <label>Active</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="status"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" name="id" hidden />
                    <button type="button" class="btn dark btn-outline btn-close" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn green">Save changes</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
