<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function my_profile() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
        );

        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_inbox() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load class
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'),
            static_url('templates/metronics/assets/global/plugins/fancybox/source/jquery.fancybox.css'),
            static_url('templates/metronics/assets/global/css/plugins.min.css'),
            static_url('templates/metronics/assets/apps/css/inbox.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_task() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function lock_screen() {
        $this->oreno_auth->lock_screen();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_lock_screen.phtml', $data);
    }

    public function un_lock_screen() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->oreno_auth->unlock_screen($post);
            if ($res) {
                echo return_call_back('message', array('verify' => true), 'json');
            } else {
                echo return_call_back('message', array('verify' => false), 'json');
            }
        } else {
            echo return_call_back('message', array('verify' => false), 'json');
        }
        exit();
    }

    public function get_user_profile() {
        $this->load->model(array('Tbl_users'));
        $result = $this->Tbl_users->find('first', array(
            'fields' => array('a.*', 'b.group_id', 'c.name group_name', 'd.address', 'd.lat', 'd.lng', 'd.zoom', 'd.facebook', 'd.twitter', 'd.instagram', 'd.linkedin', 'd.img'),
            'conditions' => array('a.id' => base64_decode($this->auth_config->user_id)),
            'joins' => array(
                array(
                    'table' => 'tbl_user_groups b',
                    'conditions' => 'b.user_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_groups c',
                    'conditions' => 'c.id = b.group_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_user_profiles d',
                    'conditions' => 'd.user_id = a.id',
                    'type' => 'left'
                )
            )
                )
        );
        $return = array('result' => array('content' => 'data not found ', 'param' => $this->auth_config->user_id, 'status' => 'failed'));
        if (isset($result) && !empty($result)) {
            $return = array('result' => array('content' => $result, 'status' => 'success'));
        } else {
            $return = array('result' => array('content' => 'data not found ', 'param' => $this->auth_config->user_id, 'status' => 'failed'));
        }
        echo json_encode($return['result']);
    }

    public function app_inbox_inbox() {
        $data['title_for_layout'] = 'welcome';
        $this->parser->parse('layouts/pages/empty.phtml', $data);
    }

    public function app_inbox_view() {
        $data['title_for_layout'] = 'welcome';
        $this->parser->parse('layouts/pages/empty.phtml', $data);
    }

    public function app_inbox_compose() {
        $data['title_for_layout'] = 'welcome';
        $this->parser->parse('layouts/pages/empty.phtml', $data);
    }

    public function app_inbox_reply() {
        $data['title_for_layout'] = 'welcome';
        $this->parser->parse('layouts/pages/empty.phtml', $data);
    }

}
