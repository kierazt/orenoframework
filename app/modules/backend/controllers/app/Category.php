<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MY_Controller {

//put your code here
    public function __construct() {
        parent::__construct();
        $this->view_path = $this->get_view_path();
        $this->load->model(array('Tbl_cms_categories'));
    }

    public function index() {
        redirect(base_developer_url('app/category/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['content'] = 'ini kontent web';
        $data['view-header-title'] = 'View Group List';
        $data['view_path'] = $this->view_path;

        //<!-- BEGIN PAGE LEVEL PLUGINS -->
        $data['load_js'][] = '<script src="' . static_url('includes/templates/metronics/assets/global/scripts/datatable.js') . '" type="text/javascript"></script>';
        $data['load_js'][] = '<script src="' . static_url('includes/templates/metronics/assets/global/plugins/datatables/datatables.min.js') . '" type="text/javascript"></script>';
        $data['load_js'][] = '<script src="' . static_url('includes/templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '" type="text/javascript"></script>';

        $data['load_js'][] = '<script src="' . static_url('includes/templates/metronics/assets/global/plugins/morris/morris.min.js') . '" type="text/javascript"></script>';
        $this->parser->parse('layout/metronic', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $this->load->library('pagination');
            $get = $this->input->get();
            $cond_count = array();
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('name', $search);
                $cond['or_like'] = array('is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['fields'] = array('*');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $total_rows = $this->Tbl_cms_categories->find('count', $cond_count);
            $config = array(
                'base_url' => base_developer_url('app/category/view/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_cms_categories->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-category">
									<div class="col-md-9" style="height:30px">
										<input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
									</div>
								</div>';
                    $data['rowcheck'] = '<input type="checkbox" class="select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />';
                    //'<div class="checkbox"><input type="checkbox" class="minimal-red checkbox_act" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" id="select_tr' . $d['id'] . '" value="' . $d['id'] . '"/></div>';
                    $data['num'] = $i;
                    $data['name'] = $d['name']; //optional		
                    $data['active'] = $action_status; //optional					
                    $data['description'] = $d['description']; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data($id = null) {
        $res = $this->Tbl_cms_categories->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if (isset($res) && !empty($res)) {
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) $this->auth->_user_id,
                'create_date' => date_now()
            );
            $res = $this->Tbl_cms_categories->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status
            );
            $res = $this->Tbl_cms_categories->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_cms_categories->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $res = $this->Tbl_cms_categories->remove($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function delete($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $res = $this->Tbl_cms_categories->delete($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

}
