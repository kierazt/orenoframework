<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Email_layout
 *
 * @author root
 */
class Email_layout extends MY_Controller{

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_email_layout'));
    }

    public function index() {
        redirect(base_backend_url('settings/email_layout/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Group List';
        $data['content'] = 'ini kontent web';
		$css_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-summernote/summernote.css'),
        );
        $this->load_js($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-markdown/lib/markdown.js'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'),
            static_url('templates/metronics/assets/global/plugins/bootstrap-summernote/summernote.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = 'Tbl_email_layout';
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('a.keyword', $search);
                $cond['or_like'] = array('a.is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['fields'] = array('a.*');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $total_rows = $this->Tbl_email_layout->find('count', $cond_count);
            $config = array(
                'base_url' => base_backend_url('settings/email_layout/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_email_layout->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $data['rowcheck'] = '<input type="checkbox" class="select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />';
                    $data['num'] = $i;
                    $data['keyword'] = $d['keyword']; //optional
                    $data['value'] = $d['value']; //optional		
                    $data['active'] = $action_status; //optional	
                    $data['description'] = $d['description']; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data($id = null) {
        $res = $this->Tbl_email_layout->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if (isset($res) && !empty($res)) {
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'keyword' => $post['keyword'],
                'value' => $post['value'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) $this->auth->_user_id,
                'create_date' => date_now()
            );
            $result = $this->Tbl_email_layout->insert($arr_insert);
            if ($result == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'keyword' => $post['keyword'],
                'value' => $post['value'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_email_layout->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_email_layout->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $res = $this->Tbl_email_layout->remove($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function delete($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $res = $this->Tbl_email_layout->delete($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

}
