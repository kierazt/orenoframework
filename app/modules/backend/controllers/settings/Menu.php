<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * 
 */
class Menu extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_menus'));
    }

    public function index() {
        redirect(base_backend_url('settings/menu/view/'));
    }

    public function view() {
        $this->load->model(array('Tbl_icons', 'Tbl_modules'));
        $data['title_for_layout'] = 'welcome';
        $data['content'] = 'ini kontent web';
        $data['view-header-title'] = 'View Menu List';
        $data['modules'] = $this->Tbl_modules->find('list', array('conditions' => array('is_active' => 1), 'order' => array('key' => 'id', 'type' => 'ASC')));

        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/jstree/dist/themes/default/style.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/jstree/dist/jstree.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_data($id = null) {
        $res = $this->Tbl_menus->find('first', array(
            'conditions' => array('tbl_menus.id' => $id),
            'fields' => array('tbl_menus.*', 'tbl_icons.id AS icon_id', 'tbl_icons.name AS icon_name'),
            'joins' => array(
                array(
                    'table' => 'tbl_icons',
                    'conditions' => 'tbl_icons.id = tbl_menus.icon_id',
                    'type' => 'left'
                )
            )
                )
        );
        $p = explode('/', $res['path']);
        $module = $p[0];
        $path = $this->fnReshapePath($res['path'], array('replace', $p[0], ''));
        if (isset($res) && !empty($res)) {
            $arr = array(
                'id' => $res['id'],
                'name' => $res['name'],
                'module' => $module,
                'path' => $path,
                'icon' => $res['icon_name'],
                'icon_img' => '<i id="icon_img" class="fa fa-fw ' . $res['icon_name'] . ' fa-3x"></i>'
            );
            echo json_encode($arr);
        } else {
            echo null;
        }
    }

    public function get_all_data() {
        $res = $this->Tbl_menus->find('first', array(
            'conditions' => array('tbl_menus.active' => q),
            'fields' => array('tbl_menus.*', 'tbl_icons.id AS icon_id', 'tbl_icons.name AS icon_name'),
            'joins' => array(
                array(
                    'table' => 'tbl_icons',
                    'conditions' => 'tbl_icons.id = tbl_menus.icon_id',
                    'type' => 'left'
                )
            )
                )
        );
        $p = explode('/', $res['path']);
        $module = $p[0];
        $path = $this->fnReshapePath($res['path'], array('replace', $p[0], ''));
        if (isset($res) && !empty($res)) {
            $arr = array(
                'id' => $res['id'],
                'name' => $res['name'],
                'module' => $module,
                'path' => $path,
                'icon' => $res['icon_name'],
                'icon_img' => '<i id="icon_img" class="fa fa-fw ' . $res['icon_name'] . ' fa-3x"></i>'
            );
            echo json_encode($arr);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post['menu']) && !empty($post['menu'])) {
            if ($post['menu']['path'] != '' || $post['menu']['path'] != '#') {
                $module_master = $this->Tbl_modules->find('first', array('conditions' => array('id' => $post['menu']['module'])));
                $path = $module_master['name'] . '/' . $post['menu']['path'];
            }
            $arr_insert = array(
                'name' => replace_to($post['menu']['name'], 'ucfirst'),
                'path' => replace_to($path, 'strtolower'),
                'icon_id' => $post['menu']['icon'],
                'module_master_id' => $post['menu']['module'],
                'active' => 1,
                'created_by' => (int) $this->auth->_user_id,
                'create_date' => date_now()
            );
            $res = $this->Tbl_menus->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function insert_form_menu_lv_2() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $path = $post['menu']['path'];
            $arr_insert = array(
                'name' => replace_to($post['menu']['name'], 'ucfirst'),
                'path' => replace_to($path, 'strtolower'),
                'icon_id' => $post['menu']['icon'],
                'menu_id' => $post['menu']['id'],
                'active' => 1,
                'created_by' => (int) $this->auth->_user_id,
                'create_date' => date_now()
            );
            $res = $this->Tbl_menu_childs->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function insert_form_menu_lv_3() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            d($post);
            $path = $post['menu']['path'];
            $arr_insert = array(
                'name' => replace_to($post['menu']['name'], 'ucfirst'),
                'path' => replace_to($path, 'strtolower'),
                'icon_id' => $post['menu']['icon'],
                'menu_id' => $post['menu']['id'],
                'menu_child_id' => $post['menu']['id'],
                'active' => 1,
                'created_by' => (int) $this->auth->_user_id,
                'create_date' => date_now()
            );
            $res = $this->Tbl_menu_grandchilds->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (isset($post['module']['controller_name']) && !empty($post['module']['controller_name'])) {
                $arr_controller = array(
                    'name' => replace_to($post['module']['controller_name'], 'ucfirst'),
                    'path' => replace_to($post['module']['controller_path'], 'strtolower')
                );
                $this->Tbl_module_controllers->update($arr_controller, $post['module']['controller_id']);
            }
            if (isset($post['module']['model_name']) && !empty($post['module']['model_name'])) {
                $arr_model = array(
                    'name' => replace_to($post['module']['model_name'], 'ucfirst'),
                    'path' => replace_to($post['module']['model_path'], 'strtolower')
                );
                $model_id = $this->Tbl_module_models->update($arr_model, $post['module']['model_id']);
            }
            if (isset($post['module']['view_name']) && !empty($post['module']['view_name'])) {
                $path_html = $post['module']['path_html'];
                $path_html_short = $post['module']['path_html'];
                $path_js = $post['module']['path_js'];
                $path_js_short = $post['module']['path_js_short'];
                $arr_view = array(
                    'name' => replace_to($post['module']['view_name'], 'ucfirst'),
                    'path_html' => $path_html,
                    'path_html_short' => $path_html_short,
                    'path_js' => $path_js,
                    'path_js_short' => $path_js_short
                );
                $this->Tbl_module_views->update($arr_view, $post['module']['view_id']);
            }
            $arr_insert = array(
                'name' => $post['module']['name'],
                'controller_id' => $post['module']['controller_id'],
                'model_id' => $post['module']['model_id'],
                'view_id' => $post['module']['view_id'],
            );
            $res = $this->Tbl_modules->update($arr_insert, $post['module']['id']);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function get_controller_prefixs($id = null) {
        if ($id != null) {
            $this->load->model('Tbl_modules');
            $res = $this->Tbl_modules->find('first', array('conditions' => array('id' => $id)));
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function get_icon($id = null) {
        if ($id != null) {
            $this->load->model('Tbl_icons');
            $res = $this->Tbl_icons->find('first', array('conditions' => array('id' => $id)));
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function get_menu($id = null, $key = 0, $name = null) {
        $menus = $this->menu($id, false, $key, $name);
        if (isset($menus) && !empty($menus)) {
            $arr = $child = $grandchild = array();
            foreach ($menus['nodes'] AS $row) {
                if (isset($row['nodes']) && !empty($row['nodes'])) {
                    foreach ($row['nodes'] AS $row2) {
                        if (isset($row2['nodes']) && !empty($row2['nodes'])) {
                            foreach ($row2['nodes'] AS $row3) {
                                $grandchild[] = array(
                                    'text' => $row3['text'],
                                    'href' => $row3['root_path'],
                                    'level' => $row2['level'],
                                    'id' => (int) $row3['id'],
                                    'child_id' => (int) $row2['id'],
                                    'child_name' => $row2['text'],
                                    'menu_id' => (int) $row['id'],
                                    'menu_name' => $row['text']
                                );
                            }
                        }

                        $child[] = array(
                            'text' => $row2['text'],
                            'href' => $row2['root_path'],
                            'level' => $row2['level'],
                            'root_id' => (int) $row2['root_id'],
                            'menu_id' => (int) $row['id'],
                            'menu_name' => $row['text'],
                            'children' => $grandchild
                        );
                    }
                }
                $arr[] = array(
                    'text' => $row['text'],
                    'href' => $row['root_path'],
                    'level' => $row['level'],
                    'root_id' => (int) $row['root_id'],
                    'children' => $child
                );
            }

            $res = array(
                'text' => $menus['text'],
                'href' => isset($menus['root_path']) ? $menus['root_path'] : '',
                'level' => $menus['level'],
                'id' => (int) $menus['id'],
                'children' => $arr
            );
            //echo json_encode($arr);
            if ($res != null) {
                echo '[' . json_encode($res) . ']';
            }
        } else {
            echo '[{ "text" : "' . $name . '" }]';
        }
    }

    public function retrieve_menu() {
        echo $this->get_menu();
    }

    public function get_menu_1() {
        $arr = $this->get_menu(1);
        if ($arr != null) {
            echo '[' . $arr . ']';
        } else {
            echo '[{ "text" : "frontend" }]';
        }
    }

    public function get_menu_2() {
        $arr = $this->get_menu(2);
        if ($arr != null) {
            echo '[' . $arr . ']';
        } else {
            echo '[{ "text" : "backend" }]';
        }
    }

    public function get_menu_3() {
        $arr = $this->get_menu(3);
        if ($arr != null) {
            echo '[' . $arr . ']';
        } else {
            echo '[{ "text" : "developer" }]';
        }
    }

    public function get_module() {
        $this->load->model('Tbl_modules');
        $modules = $this->Tbl_modules->find('all', array('conditions' => array('is_active' => 1)));
        echo json_encode($modules);
    }

    public function add_menu($id = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            debug($post);
        }
    }

}
