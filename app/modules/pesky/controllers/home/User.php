<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tbl_users');
    }

    public function index() {
        $data['title_for_layout'] = 'welcome';
//        $js_files = array(
//            static_url('templates/metronics/assets/global/scripts/datatable.js'),
//            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
//            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
//        );
//        $this->load_js($js_files);
        $content = $this->get_page('home');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function about() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('about');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function auth() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $auth = $this->oreno_auth->auth($post, 'pesky');
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function contact() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('contact');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function event() {
        $data['title_for_layout'] = 'welcome';
        $css_files = array(
            static_url('templates/metronics/assets/global/css/plugins.min.css'),
        );
        $this->load_css($css_files);

        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('lib/packages/twitter-bootstrap-wizard-master/jquery.bootstrap.wizard.min.js'),
        );
        $this->load_js($js_files);
        $content = $this->get_page('event');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function get_event_list($offset = 1) {
        $this->load->model('Tbl_pesky_sport_events');
        $this->load->library('pagination');
        $perpage = 3;
        $cond['fields'] = array('*');
        $cond['limit'] = array('perpage' => $perpage, 'offset' => $offset);
        $res = $this->Tbl_pesky_sport_events->find('all', $cond);
        $cond_count = array();
        $total_rows = $this->Tbl_pesky_sport_events->find('count', $cond_count);
        if ($res == null) {
            $total_rows = 0;
        }
        $config = array(
            'base_url' => base_pesky_url('home/user/get_event_list/'),
            'total_rows' => $total_rows,
            'per_page' => $perpage,
            'num_links' => 5,
            'use_page_numbers' => TRUE
        );
        $this->pagination->initialize($config);
        $arr = '';
        if (isset($res) && !empty($res)) {
            foreach ($res as $val) {
                $arr .= '<div class="col-lg-4">
                    <div class="feature_item">
                        <img src="' . static_url("templates/maxitechture/img/icon/f-icon-1.png") . '" alt="">
                        <h4>' . $val['name'] . '<br/><small>(Event Date ' . date('d M Y', strtotime($val['event_date'])) . ')</small></h4>
                        <p>Event registration start date ' . date('d M Y', strtotime($val['date_start_registration'])) . ' And will close at ' . date('d M Y', strtotime($val['date_end_registration'])) . '</p>
                        <a class="main_btn" href="' . base_pesky_url('view-detail-event/' . $val['seo_name'] . '/' . $val['id']) . '" >View Details</a>
                        <a class="main_btn" href="' . base_pesky_url('join-event-page/' . $val['seo_name'] . '/' . $val['id']) . '" >Joins</a>
                    </div>
                </div>';
            }
        }
        $link = $this->pagination->create_links();
        if (!$link) {
            $link = 'no data found!';
        }
        $output = array(
            'link' => $link,
            'data' => $arr,
        );
        //output to json format
        echo json_encode($output);
    }

    public function gallery() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('gallery');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function login() {
        $get = $this->input->get();
        if (isset($get['message']) && !empty($get['message'])) {
            $data['message'] = $get['message'];
        }
        $data['title_for_layout'] = 'welcome';
        $var = array(
            array(
                'keyword' => '_year_range',
                'value' => date('Y', strtotime('-50Year')) . ':' . date('Y', strtotime('-10Year'))
            )
        );
        $this->load_ajax_var($var);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function logout() {
        $this->oreno_auth->destroy_session($this->_session_auth($this->config->session_pesky), $this->config->session_pesky);
        $this->session->set_flashdata('success', 'Successfully logout from system!');
        redirect(base_pesky_url('login'));
    }

    public function mail_layout() {
        $data['title_for_email'] = 'Your account successfully create at Pesky IndoTimingSport';
        $data['caption_for_email'] = 'Please click this link to activate your account.';
        $data['footer'] = 'Please do not reply this email, this is auto send by system.';
        $this->load->view('layouts/email/pesky/user_activation.phtml', $data);
    }

    public function send_mail_test() {
        $this->load->library('oreno_mail');
        $data['content'] = 'test aja';
        $from = array('me', 'firman.begin@gmail.com');
        $to = 'ariffirmansyah.begin@gmail.com';
        $options = array(
            'subject' => 'test',
            'layout' => 'layouts/email/pesky/club_activation.phtml'
        );
        $res = $this->oreno_mail->send($data, $from, $to, $options);
        debug($res);
    }

    public function send_mail_test2() {
// the message
        $msg = "First line of text\nSecond line of text";

// use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg, 70);

// send email
        mail("ariffirmansyah.begin@gmail.com", "My subject", $msg);
    }

    public function register_club() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $auth = $this->oreno_auth->register($post, 'pesky', 'club');
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function register_user() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $auth = $this->oreno_auth->register($post, 'pesky', 'user');
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function validate_club_name() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_pesky_sport_participant_clubs');
            $res = $this->Tbl_pesky_sport_participant_clubs->find('first', array('like' => array('a.seo_name', $this->oreno_seo->get_name($post['name']), 'both')));
            if (isset($res) && !empty($res)) {
                echo 'failed';
            } else {
                echo 'success';
            }
        }
        exit();
    }

    public function validate_email($keyword) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if ($keyword == 'user') {
                $res = $this->Tbl_users->find('first', array('like' => array('a.email', base64_decode($post['email']), 'both')));
            } elseif ($keyword == 'club') {
                $this->load->model('Tbl_pesky_sport_participant_clubs');
                $res = $this->Tbl_pesky_sport_participant_clubs->find('first', array('like' => array('a.email', base64_decode($post['email']), 'both')));
            }
            if (isset($res) && !empty($res)) {
                echo 'failed';
            } else {
                echo 'success';
            }
        }
        exit();
    }

    public function join_event_page($title = null, $id = null) {
        $sess = $this->_session_auth($this->config->session_pesky);
        $data['step_start'] = 1;
        if (isset($sess) && !empty($sess) && $sess['is_logged_in'] == true) {
            $data['step_start'] = '2';
        }
        $data['title_for_layout'] = 'welcome';
        $var = array(
            array(
                'keyword' => '_year_range',
                'value' => date('Y', strtotime('-50Year')) . ':' . date('Y', strtotime('-10Year'))
            ),
            array(
                'keyword' => 'step_start',
                'value' => $data['step_start']
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            static_url('lib/packages/twitter-bootstrap-wizard-master/prettify.css')
        );
        $this->load_css($css_files);

        $js_files = array(
            static_url('lib/packages/twitter-bootstrap-wizard-master/jquery.bootstrap.wizard.min.js'),
            static_url('lib/packages/twitter-bootstrap-wizard-master/prettify.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function view_detail_event($title = null, $id = null) {
        $this->load->model('Tbl_pesky_sport_events');
        $data['title_for_layout'] = 'welcome';
        $css_files = array(
            static_url('lib/packages/twitter-bootstrap-wizard-master/prettify.css')
        );
        $this->load_css($css_files);

        $js_files = array(
            static_url('lib/packages/twitter-bootstrap-wizard-master/jquery.bootstrap.wizard.min.js'),
            static_url('lib/packages/twitter-bootstrap-wizard-master/prettify.js')
        );
        $this->load_js($js_files);
        $data['content'] = $this->Tbl_pesky_sport_events->find('first', array('conditions' => array('id' => $id)));
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

}
