<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tbl_users');
    }

    public function index() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('home');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function get_event_list() {
        $post = $this->input->post(NULL, TRUE);
        $this->load->model('Tbl_pesky_sport_events');
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = 'Tbl_pesky_sport_events';
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('a.name', $search);
                $cond['or_like'] = array('a.is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['fields'] = array('a.*');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $total_rows = $this->Tbl_pesky_sport_events->find('count', $cond_count);
            $config = array(
                'base_url' => base_pesky_url('home/user/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_pesky_sport_events->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $action = '<a class="btn red btn-block" href="' . base_pesky_url('view-detail-event/' . $d['seo_name'] . '/' . $d['id']) . '" >Detail</a>
                        <a class="btn red btn-block" href="' . base_pesky_url('join-event-page/' . $d['seo_name'] . '/' . $d['id']) . '" >Join</a>
                    ';
                    $data['num'] = $i;
                    $data['name'] = '<a href="#" id="detail_event" title="klik to view details event">' . $d['name'] . '</a>'; //optional		
                    $data['start'] = $d['date_start_registration']; //optional	
                    $data['end'] = $d['date_end_registration']; //optional	
                    $data['event_date'] = $d['event_date']; //optional	
                    $data['description'] = substr($d['description'], 0, 100); //optional
                    $data['total_participant'] = '<a href="#" id="total_participant" title="klik to view details event participant">' . 0 . '</a>';
                    $data['action'] = $action; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function event() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('dashboard/event');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function profile() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('gallery');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function history() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('gallery');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function news() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('gallery');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

    public function social() {
        $data['title_for_layout'] = 'welcome';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $content = $this->get_page('gallery');
        if (isset($content['content']) && !empty($content['content'])) {
            $data['content_text'] = $content['content'];
        }
        $this->parser->parse('layouts/pages/maxitechture.phtml', $data);
    }

}
