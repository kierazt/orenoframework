<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('event' + ' is ready');
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_pesky_url + 'dashboard/user/get_event_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "name"},
                        {"data": "start"},
                        {"data": "end"},
                        {"data": "event_date"},
                        {"data": "total_participant"},
                        {"data": "description"},
                        {"data": "action"}
                    ]
                });

            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });
</script>