<div class="feature_inner row">
    <div class="col-lg-4">
        <div class="feature_item">
            <img src="<?php echo static_url('templates/maxitechture/img/icon/f-icon-1.png')?>" alt="">
            <h4>Sign As Athlete</h4>
            <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each.</p>
            <a class="main_btn" href="#">View Details</a>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="feature_item">
            <img src="<?php echo static_url('templates/maxitechture/img/icon/f-icon-2.png')?>" alt="">
            <h4>Sign As Volunteer</h4>
            <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each.</p>
            <a class="main_btn" href="#">View Details</a>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="feature_item">
            <img src="<?php echo static_url('templates/maxitechture/img/icon/f-icon-3.png')?>" alt="">
            <h4>Sign As Supporter</h4>
            <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each.</p>
            <a class="main_btn" href="#">View Details</a>
        </div>
    </div>
</div>