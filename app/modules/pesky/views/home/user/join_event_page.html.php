<div id='step1'>
    <h3 style='border-bottom: 1px solid #ccc'>Step 1</h3>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>Register & Join</b></span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">First name</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="First name" name="first_name" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Last Name</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Last Name" name="last_name" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Birth Date</label>
                <input class="form-control form-control-solid placeholder-no-fix birthdate" type="text" name="birthdate" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix email_register" type="text" autocomplete="off" placeholder="Email" name="email" id="email_register_user"/> 
            </div>
        </div>
        <div class="col-md-6">         
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>Login & Join</b></span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> 
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password_login" /> 
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <center>
            <div id="next" data-step='1' class="btn red btn-block uppercase col-2 next" >Next</div>
        </center>
    </div>
</div>

<div id='step2'>
    <h3 style='border-bottom: 1px solid #ccc'>Step 2</h3>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>Your Profile</b></span>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <center>
            <div id="next" data-step='2' class="btn red btn-block uppercase col-2 next" >Next</div>
        </center>
    </div>
</div>