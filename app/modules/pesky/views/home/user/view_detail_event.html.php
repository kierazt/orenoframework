<aside class="f_widget news_widget">
    <div class="feature_inner row event_list">
        <div class="col-lg-12">
            <div class="feature_item" style='overflow: auto'>
                <div class='col-md-6'>
                    <img src="<?php echo static_url("templates/maxitechture/img/icon/f-icon-1.png"); ?>" alt="">
                    <h4><?php echo $content['name']; ?><br/><small>(Event Date <?php echo date('d M Y', strtotime($content['event_date'])); ?>)</small></h4>
                    <p>Event registration start date <?php echo date('d M Y', strtotime($content['date_start_registration'])); ?> And will close at <?php echo date('d M Y', strtotime($content['date_end_registration'])); ?></p>
                    <a class="main_btn" href="<?php echo base_pesky_url('join-event-page/' . $content['seo_name'] . '/' . $content['id']); ?>" >Joins</a>
                </div>

                <div class='col-md-6'>
                    <h4>Description</h4>
                    <p><?php echo $content['description']; ?></p>
                </div>
            </div>
        </div>
    </div> 
</aside>