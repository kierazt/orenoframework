<script>
    var validate = function () {
        var username = $('input[name="username"]').val();
        var password = $('input[name="password_login"]').val();
        var res = '';
        if (username == '') {
            res += 'Username is required!!!';
        }
        if (password == '') {
            res += 'Password is required!!!';
        }
        return res;
    };

    var AjaxSub = function () {
        return {
            init: function () {
                toastr.success('login ajax is ready');
                $(".birthdate").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    yearRange: _year_range,
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 999999);
                        }, 0);
                    }
                });

                $('#club_name').on('change', function () {
                    App.startPageLoading({animate: true});
                    var value = $(this).val();
                    var formdata = {
                        name: value
                    };
                    var uri = base_pesky_url + 'validate-club-name';
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            if (response == 'failed') {
                                fnToStr('Similar club name is already registered.', 'warning', 2100);
                            } else {
                                fnToStr('Club name is not yet registered, process is to continue.', 'success', 2100);
                            }
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Validate email address is failed', 'warning', 2100);
                        }

                    });
                    return false;
                });

                $('#email_register_user').on('change', function () {
                    App.startPageLoading({animate: true});
                    var value = $(this).val();
                    var formdata = {
                        email: Base64.encode(value)
                    };
                    var uri = base_pesky_url + 'validate-email/user';
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            if (response == 'failed') {
                                App.stopPageLoading();
                                $('#email_register_user').val('');
                                $('input[name="password"]').val('');
                                document.getElementById("email_register_user").placeholder = value;
                                $('#email_register_user').css("border", "red");
                                fnToStr('Email address is already registered, process cannot continue.', 'warning', 2100);
                            } else {
                                fnToStr('Email address is not exist at our databse, process is to continue.', 'success', 2100);
                            }
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Validate email address is failed', 'warning', 2100);
                        }

                    });
                    return false;
                });
                $('#email_register_club').on('change', function () {
                    App.startPageLoading({animate: true});
                    var value = $(this).val();
                    var formdata = {
                        email: Base64.encode(value)
                    };
                    var uri = base_pesky_url + 'validate-email/club';
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            if (response == 'failed') {
                                App.stopPageLoading();
                                $('#email_register_club').val('');
                                $('input[name="password"]').val('');
                                document.getElementById("email_register_club").placeholder = value;
                                $('#email_register_club').css("border", "red");
                                fnToStr('Email address is already registered, process cannot continue.', 'warning', 2100);
                            } else {
                                fnToStr('Email address is not exist at our databse, process is to continue.', 'success', 2100);
                            }
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Validate email address is failed', 'warning', 2100);
                        }

                    });
                    return false;
                });

                $('#pswd2').on('change', function () {
                    var value = $(this).val();
                    var pswd = $('#pswd').val();
                    if (value != pswd) {
                        fnToStr('Your passwod not match, please try again with correct one!', 'warning', 2100);
                    } else {
                        fnToStr('Your passwod match', 'success', 2100);
                    }
                    return false;
                });

                $('#submit').on('click', function () {
                    App.startPageLoading({animate: true});
                    var username = $('input[name="username"]').val();
                    var password = $('input[name="password_login"]').val();
                    var formdata = {
                        userid: username,
                        password: Base64.encode(password)
                    };
                    var uri = base_pesky_url + 'auth-user';
                    if (validate() != '') {
                        fnToStr(validate(), 'warning', 1100);
                        return false;
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            fnToStr('Successfully login into dashboard', 'success', 1100);
                            setTimeout(function () {
                                App.stopPageLoading();
                                window.location.replace(base_pesky_url + 'dashboard');
                            }, 1100);
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Failed login into dashboard!', 'warning', 1100);
                        }

                    });
                    return false;
                });
                $('#submit_register-participant-form').on('click', function () {
                    App.startPageLoading({animate: true});
                    var fname = $('input[name="first_name"]').val();
                    var lname = $('input[name="last_name"]').val();
                    var email = $('input[name="email"]').val();
                    var pass1 = $('input[name="password"]').val();
                    var pass2 = $('input[name="password2"]').val();
                    var formdata = {
                        first_name: fname,
                        last_name: lname,
                        email: email,
                        password: pass1
                    };
                    var uri = base_pesky_url + 'register-user';
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            fnToStr('Successfully create account in pesky IndoSportTiming, please check your emaill address for activate your account.', 'success', 1100);
                            setTimeout(function () {
                                App.stopPageLoading();
                                window.location.replace(base_pesky_url + 'login?message="successfully registered your account into system."');
                            }, 1100);
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Failed create account in pesky IndoSportTiming', 'warning', 1100);
                        }

                    });
                    return false;
                });

                $('#submit_register-club-form').on('click', function () {
                    App.startPageLoading({animate: true});
                    var fname = $('input[name="club_name"]').val();
                    var lname = '';
                    var manager_name = $('input[name="manager_name"]').val();
                    var email = $('input[name="email"]').val();
                    var pass1 = $('input[name="password"]').val();
                    var pass2 = $('input[name="password2"]').val();
                    if (pass1 != pass2) {
                        fnToStr('Password not matched', 'warning', 1100);
                        return false;
                    }
                    var formdata = {
                        first_name: fname,
                        last_name: lname,
                        manager_name: manager_name,
                        email: email,
                        password: pass1
                    };
                    var uri = base_pesky_url + 'register-club';
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            fnToStr('Successfully create account in pesky IndoSportTiming, please check your emaill address for activate your account.', 'success', 1100);
                            setTimeout(function () {
                                App.stopPageLoading();
                                window.location.replace(base_pesky_url + 'login?message="successfully registered your account into system. Please check your email to activate club account."');
                            }, 1100);
                        },
                        error: function () {
                            App.stopPageLoading();
                            fnToStr('Failed create account in pesky IndoSportTiming', 'warning', 1100);
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        AjaxSub.init();
    });

</script>