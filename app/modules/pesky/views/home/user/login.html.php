<?php if (isset($message) && !empty($message)): ?>
    <center style="width:50%; margin:0px auto; margin-bottom:40px"><h3><?php echo $message; ?></h3></center>
<?php endif; ?>
<div class="col-md-12">
    <div class="col-md-3">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" id="login-form">
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>User Login</b></span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> 
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password_login" /> 
            </div>
            <div class="form-actions">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%; text-align:left">
                            <button id="submit" type="submit" class="btn red btn-block uppercase" style="width:50%">Login</button>
                        </td>
                        <td style="width:50%; text-align:right">
                            <input type="checkbox" name="remember" value="1" />Remember me </label>					
                        </td>
                    </tr>
                </table>
            </div>
            <div class="form-actions">
                <div class="pull-right forget-password-block">
                    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
            </div>
        </form>
    </div>    
    <div class="col-md-4">
        <form class="register-club-form">
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>Club Registration.</b></span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Club name</label>
                <input class="form-control form-control-solid placeholder-no-fix club_name" id="club_name" type="text" autocomplete="off" placeholder="Club name" name="club_name" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Manager Name</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Manager Name" name="manager_name" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix email_register" type="text" autocomplete="off" placeholder="Email" name="email" id="email_register_club"/> 
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" id="pswd" type="password" autocomplete="off" placeholder="Password" name="password" />
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Re-Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" id="pswd2" type="password" autocomplete="off" placeholder="Password" name="password2" />
            </div>
            <div class="form-actions">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%; text-align:left">
                            <button type="submit" id="submit_register-club-form" class="btn red btn-block uppercase" style="width:25%">Register</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <div class="col-md-5">
        <!-- BEGIN LOGIN FORM -->
        <form class="register-participant-form">
            <div class="form-title">
                <span class="form-title" style="font-size:24px"><b>User Registration.</b></span>
            </div>
            <div class="form-group col-md-6">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">First name</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="First name" name="first_name" /> 
            </div>
            <div class="form-group col-md-6">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Last Name</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Last Name" name="last_name" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Birth Date</label>
                <input class="form-control form-control-solid placeholder-no-fix birthdate" type="text" name="birthdate" /> 
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix email_register" type="text" autocomplete="off" placeholder="Email" name="email" id="email_register_user"/> 
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" id="pswd" type="password" autocomplete="off" placeholder="Password" name="password" />
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Re-Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" id="pswd2" type="password" autocomplete="off" placeholder="Password" name="password2" />
            </div>
            <div class="form-actions">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%; text-align:left">
                            <button type="submit" id="submit_register-participant-form" class="btn red btn-block uppercase" style="width:20%">Register</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</div>