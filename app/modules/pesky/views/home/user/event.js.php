<script>
    var fnShowDataEvent = function (page) {
        if (!page) {
            page = 1;
        }
        var uri = base_pesky_url + 'home/user/get_event_list/' + page;
        $.ajax({
            url: uri,
            method: "POST", //First change type to method here
            success: function (response) {
                var res = JSON.parse(response);
                $('.event_list').html(res.data);
                $('#pagination_ajax').html(res.link);
            },
            error: function (response) {
                $('.event_list').html(response);
                $('#pagination_ajax').html('');
            }

        });
        return false;
    };
    
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('event' + ' is ready');
                fnShowDataEvent();
                $('#pagination_ajax').on('click', 'a', function (e) {
                    e.preventDefault();
                    var pageNum = $(this).attr('data-ci-pagination-page');
                    fnShowDataEvent(pageNum);
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });
</script>