<script>
    var fnInitStep = function () {
        if (step_start == 1) {
            var aft_step = parseInt(step_start) + 1;
            $('#step' + aft_step).hide();
            $('#step' + step_start).show();
        } else if (step_start == 2) {
            var bfr_step = step_start - 1;
            $('#step' + bfr_step).hide();
            $('#step' + step_start).show();
        }
    };

    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('join event js is ready');
                fnInitStep();
                $('#rootwizard').bootstrapWizard();
                $(".birthdate").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    yearRange: _year_range,
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 999999);
                        }, 0);
                    }
                });
                $('#next').on('click', function () {
                    var step = $(this).attr('data-step');
                    if (step == 1) {
                        var fname = $('input[name="first_name"]').val();
                        var lname = $('input[name="last_name"]').val();
                        var birthdate = $('input[name="birthdate"]').val();
                        var email = $('input[name="email"]').val();
                        var username = $('input[name="username"]').val();
                        var password = $('input[name="password_login"]').val();
                        var form_data = {
                            register: {
                                fname: fname,
                                lname: lname,
                                birthdate: birthdate,
                                email: email
                            }
                        };
                        if (username) {
                            form_data = {
                                login: {
                                    username: username,
                                    password: password
                                }
                            };
                        }
                        $.ajax({
                            url: uri,
                            method: "POST", //First change type to method here
                            data: formdata,
                            success: function (response) {
                                App.stopPageLoading();
                                if (response == 'failed') {
                                    fnToStr('Similar club name is already registered.', 'warning', 2100);
                                } else {
                                    fnToStr('Club name is not yet registered, process is to continue.', 'success', 2100);
                                }
                            },
                            error: function () {
                                App.stopPageLoading();
                                fnToStr('Validate email address is failed', 'warning', 2100);
                            }

                        });

                    } else if (step == 2) {

                    } else if (step == 3) {

                    }
                });
            }
        };
    }();
    jQuery(document).ready(function () {
        Ajax.init();
    });
</script>