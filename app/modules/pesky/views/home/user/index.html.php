<?php if ($content_text) : ?>
    <section class="p_20">
        <div class="container">
            <div class="main_title">
                <h2><?php echo $content_text['title']; ?></h2>
                <p><?php echo $content_text['text']; ?></p>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="p_20">
    <div class="container">            
        <div class="main_title">
            <h2>Event Sport Recomendations from us.</h2>
            <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.</p>
        </div>
        <div class="testi_slider owl-carousel">
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-1.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-2.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-1.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-2.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-1.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testi_item">
                    <div class="media">
                        <div class="d-flex">
                            <img src="<?php echo static_url('templates/maxitechture/img/testimonials/testi-2.png'); ?>" alt="">
                        </div>
                        <div class="media-body">
                            <p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware.</p>
                            <h4>Mark Alviro Wiens</h4>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p_20" style="margin-top:80px">
    <div class="container">
        <div class="main_title">
            <div class="clients_slider owl-carousel">
                <div class="item">
                    <img src="<?php echo static_url('templates/maxitechture/img/clients-logo/c-logo-1.png'); ?>" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo static_url('templates/maxitechture/img/clients-logo/c-logo-2.png'); ?>" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo static_url('templates/maxitechture/img/clients-logo/c-logo-3.png'); ?>" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo static_url('templates/maxitechture/img/clients-logo/c-logo-4.png'); ?>" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo static_url('templates/maxitechture/img/clients-logo/c-logo-5.png'); ?>" alt="">
                </div>
            </div>
        </div></div>
</section>
