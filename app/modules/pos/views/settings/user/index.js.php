<script>
	var SetFocus = function  () {
		var input = document.getElementById ("input_global");
		input.focus();
	}
    var AjaxSub = function () {
        return {
            init: function () {
                toastr.success('index ajax is ready');
				
				SetFocus();
				
				$("#total_payment").inputmask('999.999.999.999', {
					numericInput: true
				}); 
				
				var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_pos_url + 'settings/user/get_list/',
                        type: 'POST',
						data: [
							{session_transaction: session_transaction}
						]
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "sku"},
                        {"data": "name"},
                        {"data": "photos"},
                        {"data": "options"},
                        {"data": "qty"},
                        {"data": "price"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });
				
                $('.frm_global_input').on('submit', function () {
                    var input_global = $('input[name="input_global"]').val();
                    var transaction_id = $('input[name="transaction_id"]').val();
                    var uri = base_pos_url + 'settings/user/insert/';
                    var txt = 'Success add new Item ...';
                    var formdata = {
                        input_global: input_global,
                        transaction_id: transaction_id
                    };
                    
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            close_modal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            close_modal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        AjaxSub.init();
    });

</script>