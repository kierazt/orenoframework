<style>
	th{
		text-align:center;
	}
</style>
<div class="row">
	<!-- END SAMPLE FORM PORTLET-->
	<!-- BEGIN SAMPLE FORM PORTLET-->
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="actions">
				<div class="btn-group">
					<button type="button" class="btn btn-default">Item Stock</button>
					<button type="button" class="btn btn-default">Transaction History</button>
					<button type="button" class="btn btn-default">Info</button>
				</div>
				<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
			</div>
		</div>
		<div class="portlet-body form">			
			<div class="col-md-8">
				<form role="form" class="form-horizontal frm_global_input">
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<div class="input-group">
								<div class="input-group-control">
									<input type="text" class="form-control" id="input_global" name="input_global" placeholder="Insert item code or name or scan barcode for add new item into p.o.s ">
									<div class="form-control-focus"> </div>
								</div>
								<span class="input-group-btn btn-right">
									<input type="text" value="{session_transaction}" hidden name="transaction_id">
									<button type="button" class="btn green-haze" aria-expanded="false">
										Add
									</button>
								</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-8">
				<div class="table-container">
					<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
						<thead>
							<tr role="row" class="heading">
								<th width="5%"> # </th>
								<th width="15%"> SKU </th>
								<th width="15%"> Name </th>
								<th width="15%"> Photos </th>
								<th width="15"> Options </th>
								<th width="15"> QTY </th>
								<th width="15"> Price </th>
								<th width="15"> Action </th>
							</tr>							
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<form role="form" class="form-horizontal">
					<div class="form-body" style="font-size:20px">
						<div class="form-group form-md-line-input has-success form-md-floating-label">
							<div class="input-icon">
								<input type="text" class="form-control" id="total_payment">
								<label for="form_control_1">Total Payment</label>
								<span class="help-block">...</span>
								<i>Rp </i>
							</div>
						</div>
						<div class="form-group form-md-line-input has-success form-md-floating-label">
							<div class="input-icon">
								<input type="text" class="form-control" disabled>
								<label for="form_control_1">Return Payment</label>
								<span class="help-block">...</span>
								<i>Rp </i>
							</div>
						</div>
						
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="form_control_1">Session ID</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="form_control_1" value="{session_transaction}" placeholder="{session_transaction}" disabled>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input has-success">
							<label class="col-md-3 control-label" for="form_control_1">Note</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="form_control_1" placeholder="Transaction Note">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="button" class="btn default">Cancel</button>
								<button type="button" class="btn red">Hold</button>
								<button type="button" class="btn blue">Finish</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>