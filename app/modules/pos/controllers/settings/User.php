<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		$code = generate_code(16);
        $data['title_for_layout'] = 'welcome';
		$data['session_transaction'] = $code;
		$js_files = array(
			static_url('templates/metronics/assets/global/plugins/typeahead/typeahead.bundle.min.js'),
			static_url('templates/metronics/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js'),
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
		
		$var = array(
            array(
                'keyword' => 'session_transaction',
                'value' => $code
            )
        );
        $this->load_ajax_var($var);
        $this->parser->parse('layouts/pages/metronic_pos.phtml', $data);
    }
	
	public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->library(array('pagination','oreno_pos'));
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);
			$cond = array(
				'conditions' => array('transaction_id' => $post[0]['session_transaction'])
			);
            $sessions = $this->oreno_pos->find('all', $cond);
            $total_rows = count($sessions);
            $config = array(
                'base_url' => base_pos_url('settings/user/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $arr = array();
            if (isset($sessions) && !empty($sessions)) {
                $i = $start + 1;
                foreach ($sessions as $session) {
					$data['num'] = $i;
                    $data['sku'] = $session['']; 
                    $data['name'] = $session['']; 
                    $data['photos'] = $session['']; 	
                    $data['options'] = $session['']; 
                    $data['qty'] = $session['']; 
                    $data['price'] = $session['']; 
                    $data['action'] = $session['']; ;
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }
	
	public function insert(){
		$post = $this->input->post(NULL, TRUE);
		$echo = '';
		if($post){
			$res = $this->oreno_pos->init_pos($post);
			if($res){
				$echo = '';
			}
		}
		echo $echo;
	}
	
	protected function get_product($by = 'all', $conditions = array()){
		$this->load->model(array('Tbl_eshop_item_trans'));
		$options['joins'] = array(
			array(
				'table' => 'tbl_eshop_items b',
				'conditions' => 'b.user_id = a.id',
				'type' => 'left'
			),
			array(
				'table' => 'tbl_groups c',
				'conditions' => 'c.id = b.group_id',
				'type' => 'left'
			),
			array(
				'table' => 'tbl_user_profiles d',
				'conditions' => 'd.user_id = a.id',
				'type' => 'left'
			)
		);
		$res = $this->Tbl_eshop_item_trans->find($by, $options);
	}

}
