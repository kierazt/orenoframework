<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//gobal router
$route['default_controller'] = 'frontend/settings/user';
$route['/'] = 'frontend/settings/user/index';
$route['home'] = 'frontend/settings/user/index';

//route backend
$route['backend'] = 'backend/settings/user/login';
$route['backend/auth-user'] = 'backend/settings/user/auth';
$route['backend/login'] = 'backend/settings/user/login';
$route['backend/logout'] = 'backend/settings/user/logout';
$route['backend/dashboard'] = 'backend/settings/user/dashboard';

//backend profiles
$route['backend/my-profile'] = 'backend/profiles/user/my_profile';
$route['backend/my-inbox'] = 'backend/profiles/user/my_inbox';
$route['backend/my-task'] = 'backend/profiles/user/my_task';
$route['backend/lock-screen'] = 'backend/profiles/user/lock_screen';
$route['backend/unlock-screen'] = 'backend/profiles/user/un_lock_screen';

//pesky global
$route['pesky'] = 'pesky/home/user/index';
$route['pesky/home'] = 'pesky/home/user/index';
$route['pesky/about'] = 'pesky/home/user/about';
$route['pesky/contact'] = 'pesky/home/user/contact';
$route['pesky/gallery'] = 'pesky/home/user/gallery';
$route['pesky/event'] = 'pesky/home/user/event';
$route['pesky/join-event-page/(:any)/(:any)'] = 'pesky/home/user/join_event_page/$1/$2';
$route['pesky/view-detail-event/(:any)/(:any)'] = 'pesky/home/user/view_detail_event/$1/$2';



//before auth user
$route['pesky/auth-user'] = 'pesky/home/user/auth';
$route['pesky/success-registered'] = 'pesky/home/user/success_registered';
$route['pesky/register-user'] = 'pesky/home/user/register_user';
$route['pesky/register-club'] = 'pesky/home/user/register_club';
$route['pesky/validate-email/(:any)'] = 'pesky/home/user/validate_email/$1';
$route['pesky/validate-club-name'] = 'pesky/home/user/validate_club_name';
$route['pesky/login'] = 'pesky/home/user/login';
$route['pesky/logout'] = 'pesky/home/user/logout';

//after auth user : if true
$route['pesky/dashboard'] = 'pesky/dashboard/user/index';
$route['pesky/dashboard/event'] = 'pesky/dashboard/user/event';
$route['pesky/dashboard/profile'] = 'pesky/dashboard/user/profile';
$route['pesky/dashboard/history'] = 'pesky/dashboard/user/history';
$route['pesky/dashboard/news'] = 'pesky/dashboard/user/news';
$route['pesky/dashboard/social'] = 'pesky/dashboard/user/social';

//pos 
$route['pos'] = 'pos/settings/user/index';
$route['pos/auth-user'] = 'pos/settings/user/auth';
$route['pos/login'] = 'pos/settings/user/login';
$route['pos/logout'] = 'pos/settings/user/logout';
$route['pos/dashboard'] = 'pos/settings/user/dashboard';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
