-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2019 at 11:48 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_orenoframework`
--
CREATE DATABASE IF NOT EXISTS `db_orenoframework` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_orenoframework`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ajax_funcs`
--

DROP TABLE IF EXISTS `tbl_ajax_funcs`;
CREATE TABLE `tbl_ajax_funcs` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ajax_funcs`
--

INSERT INTO `tbl_ajax_funcs` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'global_uri', '\'var global_uri = function(link){\r\n                return \"\' . global_uri($this->config->module_name) . \'\"+link\r\n            };\'', '-', 1, 1, '2019-01-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_categories`
--

DROP TABLE IF EXISTS `tbl_cms_categories`;
CREATE TABLE `tbl_cms_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_category_contents`
--

DROP TABLE IF EXISTS `tbl_cms_category_contents`;
CREATE TABLE `tbl_cms_category_contents` (
  `id` int(32) NOT NULL,
  `content_id` int(32) NOT NULL,
  `content_category_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_comments`
--

DROP TABLE IF EXISTS `tbl_cms_comments`;
CREATE TABLE `tbl_cms_comments` (
  `id` int(32) NOT NULL,
  `text` text NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_block` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_contents`
--

DROP TABLE IF EXISTS `tbl_cms_contents`;
CREATE TABLE `tbl_cms_contents` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  `is_footer` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '0',
  `is_page` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` int(32) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cms_contents`
--

INSERT INTO `tbl_cms_contents` (`id`, `title`, `text`, `meta_keyword`, `meta_description`, `is_footer`, `is_active`, `is_page`, `menu_id`, `description`, `created_by`, `create_date`) VALUES
(1, 'Home', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 1, '-', 1, '2018-11-14 10:35:25'),
(2, 'About', ' <aside class=\"f_widget ab_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>About Me</h3>\r\n                    </div>\r\n                    <p>If you own an Iphone, you’ve probably already worked out how much fun it is to use it to watch movies-it has that nice big screen, and the sound quality.</p>\r\n                </aside>', '', '', 1, 1, 1, 2, '-', 1, '2018-12-18 11:38:30'),
(3, 'Contact', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 3, '-', 1, '2018-11-14 10:35:25'),
(4, 'Galleries', 'The standard Lorem Ipsum passage, used since the 1500s\r\n\r\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '', '', 0, 1, 1, 4, '-', 1, '2018-12-18 11:38:30'),
(5, 'Events', '<aside class=\"f_widget news_widget\">\r\n    <div class=\"feature_inner row event_list\">\r\n\r\n</div>  <br/>\r\n\r\n<center><div id=\"pagination_ajax\"></div></center>\r\n</aside>', '', '', 1, 1, 1, 5, '-', 1, '2018-12-18 11:38:30'),
(6, 'Socials', '<aside class=\"f_widget social_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class=\"list\">\r\n                        <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>\r\n                    </ul>\r\n                </aside>', '', '', 1, 1, 1, 0, '-', 1, '2018-12-18 11:38:30'),
(7, 'Dashboard', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 11, '-', 1, '2018-11-14 10:35:25'),
(8, 'Profile', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 1, 1, 1, 12, '-', 1, '2018-12-18 11:38:30'),
(9, 'Event', '<aside class=\"f_widget news_widget\">\r\n    <div class=\"table-container\">\r\n        <table class=\"table table-striped table-bordered table-hover table-checkable\" id=\"datatable_ajax\">\r\n            <thead>\r\n                <tr role=\"row\" class=\"heading\">\r\n                    <th width=\"5%\"> # </th>\r\n                    <th width=\"15%\"> Event Name </th>\r\n                    <th width=\"15%\"> Registration Date Start  </th>\r\n                    <th width=\"15%\"> Registration Date End </th>\r\n                    <th width=\"15%\"> Event Date </th>\r\n                    <th width=\"15%\"> Total Participate </th>\r\n                    <th width=\"200\"> Description </th>\r\n                    <th width=\"200\"> Action </th>\r\n                </tr>							\r\n            </thead>\r\n            <tbody></tbody>\r\n        </table>\r\n    </div>   \r\n</aside>', '', '', 0, 1, 1, 13, '-', 1, '2018-11-14 10:35:25'),
(10, 'History', 'The standard Lorem Ipsum passage, used since the 1500s\r\n\r\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '', '', 0, 1, 1, 14, '-', 1, '2018-12-18 11:38:30'),
(11, 'News', '<aside class=\"f_widget news_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Newsletter</h3>\r\n                    </div>\r\n                    <p>Stay updated with our latest trends</p>\r\n                    <div id=\"mc_embed_signup\">\r\n                        <form target=\"_blank\" method=\"post\" class=\"subscribes\">\r\n                            <div class=\"input-group d-flex flex-row\">\r\n                                <input name=\"EMAIL\" placeholder=\"Enter email address\" onfocus=\"this.placeholder = \'\'\" onblur=\"this.placeholder = \'Email Address \'\" required=\"\" type=\"email\">\r\n                                <button class=\"btn sub-btn\"><span class=\"lnr lnr-arrow-right\"></span></button>		\r\n                            </div>				\r\n                            <div class=\"mt-10 info\"></div>\r\n                        </form>\r\n                    </div>\r\n                </aside>', '', '', 1, 1, 1, 15, '-', 1, '2018-12-18 11:38:30'),
(12, 'Socials', '<aside class=\"f_widget social_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class=\"list\">\r\n                        <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>\r\n                    </ul>\r\n                </aside>', '', '', 1, 1, 1, 16, '-', 1, '2018-12-18 11:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_content_photos`
--

DROP TABLE IF EXISTS `tbl_cms_content_photos`;
CREATE TABLE `tbl_cms_content_photos` (
  `id` int(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_messages`
--

DROP TABLE IF EXISTS `tbl_component_messages`;
CREATE TABLE `tbl_component_messages` (
  `id` int(32) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from_id` int(32) NOT NULL,
  `to_id` int(32) NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `label_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_messages`
--

INSERT INTO `tbl_component_messages` (`id`, `subject`, `content`, `from_id`, `to_id`, `status_id`, `category_id`, `label_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'test', 'wew lah pokona', 1, 2, 1, NULL, 1, 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_categories`
--

DROP TABLE IF EXISTS `tbl_component_message_categories`;
CREATE TABLE `tbl_component_message_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_labels`
--

DROP TABLE IF EXISTS `tbl_component_message_labels`;
CREATE TABLE `tbl_component_message_labels` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_message_labels`
--

INSERT INTO `tbl_component_message_labels` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'general', '-', 1, 1, '2019-02-12 00:00:00'),
(2, 'bussiness', '-', 1, 1, '2019-02-12 00:00:00'),
(3, 'news', '-', 1, 1, '2019-02-12 00:00:00'),
(4, 'important', '-', 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_status`
--

DROP TABLE IF EXISTS `tbl_component_message_status`;
CREATE TABLE `tbl_component_message_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_message_status`
--

INSERT INTO `tbl_component_message_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'unread', '-', 1, 1, '2019-02-11 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-11 00:00:00'),
(3, 'archive', '-', 1, 1, '2019-02-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notifications`
--

DROP TABLE IF EXISTS `tbl_component_notifications`;
CREATE TABLE `tbl_component_notifications` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `status_id` int(32) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_notifications`
--

INSERT INTO `tbl_component_notifications` (`id`, `title`, `content`, `description`, `label`, `category_id`, `status_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'server not responding', 'server return 500 in browser', '-', 'label-success', 2, 1, 1, 1, '2019-02-02 00:00:00'),
(2, 'Application error', 'website error when access using chrome and show php error', '-', 'label-danger', 3, 1, 1, 1, '2019-02-02 00:00:00'),
(3, 'Database overload', 'web show db overload notice and cannot access', '-', 'label-warning', 4, 1, 1, 1, '2019-02-02 00:00:00'),
(4, 'IP 12.123.2.122 user blok with server', 'IP 12.123.2.122 user blok with server', '-', 'label-info', 1, 1, 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notification_categories`
--

DROP TABLE IF EXISTS `tbl_component_notification_categories`;
CREATE TABLE `tbl_component_notification_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_notification_categories`
--

INSERT INTO `tbl_component_notification_categories` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'network', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'server', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'system', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'database', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notification_status`
--

DROP TABLE IF EXISTS `tbl_component_notification_status`;
CREATE TABLE `tbl_component_notification_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_notification_status`
--

INSERT INTO `tbl_component_notification_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'pending', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'replied', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'archive', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_tasks`
--

DROP TABLE IF EXISTS `tbl_component_tasks`;
CREATE TABLE `tbl_component_tasks` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `progress` int(3) NOT NULL,
  `description` text NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_task_categories`
--

DROP TABLE IF EXISTS `tbl_component_task_categories`;
CREATE TABLE `tbl_component_task_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_task_status`
--

DROP TABLE IF EXISTS `tbl_component_task_status`;
CREATE TABLE `tbl_component_task_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_configs`
--

DROP TABLE IF EXISTS `tbl_configs`;
CREATE TABLE `tbl_configs` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `is_static` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_configs`
--

INSERT INTO `tbl_configs` (`id`, `keyword`, `value`, `is_static`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'app_name', 'orenoframework', 0, 1, 1, '2018-07-23 00:00:00'),
(2, 'lang_id', '28173tgfds7tf', 0, 1, 1, '2018-07-23 00:00:00'),
(3, 'lang_name', 'english', 0, 1, 1, '2018-07-23 00:00:00'),
(4, 'salt', '834red567gh4765vbfr76538', 0, 1, 1, '2018-07-23 00:00:00'),
(5, 'session_name', 'd98786tayghdjaw90d87atw', 0, 1, 1, '2018-07-23 00:00:00'),
(6, 'website_id', 'd9s8a7yudhioas987dyuhss', 0, 1, 1, '2018-07-23 00:00:00'),
(7, 'cookie_id', '90daw786tyghdjioaw987d6', 0, 1, 1, '2018-07-23 00:00:00'),
(8, 'redirect_success_login_backend', 'backend/dashboard', 0, 1, 1, '2018-07-23 00:00:00'),
(9, 'redirect_failed_login_backend', 'backend/login', 0, 1, 1, '2018-07-23 00:00:00'),
(10, 'mod_active', 'blog', 0, 1, 1, '2018-11-04 00:00:00'),
(11, 'controller_active', 'home', 0, 1, 1, '2018-11-04 00:00:00'),
(12, 'global_title_en', 'welcome to e-tracking', 0, 1, 1, '2018-11-09 07:53:48'),
(13, 'layout_frontend', 'maxitechture', 0, 1, 1, '2018-11-16 20:32:48'),
(14, 'uri_img_item_color', 'media/img/items/colors/', 0, 1, 1, '2019-01-03 09:45:11'),
(15, 'uri_img_item_brand', 'media/img/items/brands/', 0, 1, 1, '2019-01-03 09:45:41'),
(16, 'dev_status', '1', 0, 1, 1, '2019-02-01 00:00:00'),
(17, 'footer_about', ' <aside class=\"f_widget ab_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>About Me</h3>\r\n                    </div>\r\n                    <p>If you own an Iphone, you’ve probably already worked out how much fun it is to use it to watch movies-it has that nice big screen, and the sound quality.</p>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(18, 'footer_newsletter', '<aside class=\"f_widget news_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Newsletter</h3>\r\n                    </div>\r\n                    <p>Stay updated with our latest trends</p>\r\n                    <div id=\"mc_embed_signup\">\r\n                        <form target=\"_blank\" method=\"post\" class=\"subscribes\">\r\n                            <div class=\"input-group d-flex flex-row\">\r\n                                <input name=\"EMAIL\" placeholder=\"Enter email address\" onfocus=\"this.placeholder = \'\'\" onblur=\"this.placeholder = \'Email Address \'\" required=\"\" type=\"email\">\r\n                                <button class=\"btn sub-btn\"><span class=\"lnr lnr-arrow-right\"></span></button>		\r\n                            </div>				\r\n                            <div class=\"mt-10 info\"></div>\r\n                        </form>\r\n                    </div>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(19, 'footer_socials', '<aside class=\"f_widget social_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class=\"list\">\r\n                        <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>\r\n                    </ul>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(20, 'session_pesky', 'd98789iu8ghdjaw90d80po9', 0, 1, 1, '2018-07-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_configs`
--

DROP TABLE IF EXISTS `tbl_email_configs`;
CREATE TABLE `tbl_email_configs` (
  `id` int(32) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `port` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `mailtype` varchar(255) NOT NULL,
  `charset` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_configs`
--

INSERT INTO `tbl_email_configs` (`id`, `protocol`, `host`, `port`, `user`, `pass`, `mailtype`, `charset`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'smtp', 'smtp.gmail.com', '587', 'firman.begin@gmail.com', 'Ab1234abcd', 'html', 'iso-8859-1', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_layout`
--

DROP TABLE IF EXISTS `tbl_email_layout`;
CREATE TABLE `tbl_email_layout` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_layout`
--

INSERT INTO `tbl_email_layout` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'user_activation', ' <center>[date]</center><br/>                         Pengguna yang terhormat,                         <br/>                         <br/>                         Terima kasih telah melakukan registrasi akun di pesky indosporttiming, berikut detail data akun anda :                         email       : [email]<br/>                         username    : [username]<br/>                         password    : [password]<br/>                         status      : tidak aktif<br/>                         <br/>                             Untuk aktivasi account klik <b>[activation_link]</b><br/>                         Akun yang belum di aktifkan tidak akan bisa melakukan pendaftaran event lomba atau login kedalam dashboard indosporttiming<br/>                         Mohon untuk tidak men-sharing atau berbagi pakai dengan pihak lain terhadap akun dengan data diri anda agar tidak terjadi hal - hal yang tidak di inginkan.<br/>                     ', '-', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_links`
--

DROP TABLE IF EXISTS `tbl_email_links`;
CREATE TABLE `tbl_email_links` (
  `id` int(32) NOT NULL,
  `email_layout_id` int(32) NOT NULL,
  `keyword` int(255) NOT NULL,
  `value` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_items`
--

DROP TABLE IF EXISTS `tbl_eshop_items`;
CREATE TABLE `tbl_eshop_items` (
  `id` int(32) NOT NULL,
  `code` varchar(64) NOT NULL,
  `name` int(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_brands`
--

DROP TABLE IF EXISTS `tbl_eshop_item_brands`;
CREATE TABLE `tbl_eshop_item_brands` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_categories`
--

DROP TABLE IF EXISTS `tbl_eshop_item_categories`;
CREATE TABLE `tbl_eshop_item_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `parent_id` int(32) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_colors`
--

DROP TABLE IF EXISTS `tbl_eshop_item_colors`;
CREATE TABLE `tbl_eshop_item_colors` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_sizes`
--

DROP TABLE IF EXISTS `tbl_eshop_item_sizes`;
CREATE TABLE `tbl_eshop_item_sizes` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_trans`
--

DROP TABLE IF EXISTS `tbl_eshop_item_trans`;
CREATE TABLE `tbl_eshop_item_trans` (
  `id` int(32) NOT NULL,
  `item_id` int(32) NOT NULL,
  `brand_id` int(32) DEFAULT '0',
  `color_id` int(32) DEFAULT '0',
  `size_id` int(32) DEFAULT '0',
  `category_id` int(32) DEFAULT '0',
  `gender_id` int(32) NOT NULL,
  `dimension_height` int(32) NOT NULL,
  `dimension_width` int(32) NOT NULL,
  `dimension_diagonal` int(32) NOT NULL,
  `weight` int(5) NOT NULL,
  `price` double NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_item_types`
--

DROP TABLE IF EXISTS `tbl_eshop_item_types`;
CREATE TABLE `tbl_eshop_item_types` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_masters`
--

DROP TABLE IF EXISTS `tbl_eshop_masters`;
CREATE TABLE `tbl_eshop_masters` (
  `id` int(32) NOT NULL,
  `name` int(255) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(32) NOT NULL,
  `lng` varchar(32) NOT NULL,
  `zoom` int(4) NOT NULL,
  `email_general` varchar(255) NOT NULL,
  `email_sales` varchar(255) NOT NULL,
  `email_finances` varchar(255) NOT NULL,
  `phone_store` varchar(16) NOT NULL,
  `phone_mobile` varchar(16) NOT NULL,
  `fax` varchar(16) NOT NULL,
  `type_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eshop_types`
--

DROP TABLE IF EXISTS `tbl_eshop_types`;
CREATE TABLE `tbl_eshop_types` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eshop_types`
--

INSERT INTO `tbl_eshop_types` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'onlineshop', '-', 1, 1, '2019-02-07 00:00:00'),
(2, 'restaurant', '-', 1, 1, '2019-02-07 00:00:00'),
(3, 'minimarket', '-', 1, 1, '2019-02-07 00:00:00'),
(4, 'workshop', '-', 1, 1, '2019-02-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_global_variables`
--

DROP TABLE IF EXISTS `tbl_global_variables`;
CREATE TABLE `tbl_global_variables` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_groups`
--

DROP TABLE IF EXISTS `tbl_groups`;
CREATE TABLE `tbl_groups` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_groups`
--

INSERT INTO `tbl_groups` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'guest', '-', 1, 1, '2019-02-25 00:00:00'),
(2, 'superuser', '-', 1, 1, '2019-01-22 00:00:00'),
(3, 'admin', '-', 1, 1, '2019-02-12 00:00:00'),
(4, 'club', '-', 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_permissions`
--

DROP TABLE IF EXISTS `tbl_group_permissions`;
CREATE TABLE `tbl_group_permissions` (
  `id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `permission_id` int(32) NOT NULL,
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_group_permissions`
--

INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `is_allowed`, `is_public`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(2, 1, 2, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(3, 1, 3, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(4, 1, 4, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(5, 1, 5, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(6, 1, 6, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(7, 1, 7, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(8, 1, 8, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(9, 1, 9, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(10, 1, 10, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(11, 1, 11, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(12, 1, 12, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(13, 1, 13, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(14, 1, 14, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(15, 1, 15, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(16, 1, 16, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(17, 1, 17, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(18, 1, 18, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(19, 1, 19, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(20, 1, 20, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(21, 1, 21, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(22, 1, 22, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(23, 1, 23, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(24, 1, 24, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(25, 1, 25, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(26, 1, 26, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(27, 1, 27, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(28, 1, 28, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(29, 1, 29, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(30, 1, 30, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(31, 1, 31, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(32, 1, 32, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(33, 1, 33, 1, 1, 1, 1, '2019-01-23 00:00:00'),
(34, 1, 34, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(35, 1, 35, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(36, 1, 36, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(37, 1, 37, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(38, 1, 38, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(39, 1, 29, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(40, 1, 30, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(41, 1, 31, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(42, 1, 32, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(43, 1, 33, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(44, 1, 34, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(45, 1, 35, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(46, 1, 36, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(47, 1, 37, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(48, 1, 38, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(49, 1, 39, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(50, 1, 40, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(51, 1, 41, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(52, 1, 42, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(53, 1, 43, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(54, 1, 44, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(55, 1, 45, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(56, 1, 46, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(57, 1, 47, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(58, 1, 48, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(59, 1, 49, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(60, 1, 50, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(61, 1, 51, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(62, 1, 52, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(63, 1, 53, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(64, 1, 54, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(65, 1, 54, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(66, 1, 55, 1, 0, 1, 1, '2019-02-16 06:28:20'),
(67, 1, 56, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(68, 1, 57, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(69, 1, 58, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(70, 1, 59, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(71, 1, 60, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(72, 1, 61, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(73, 1, 62, 1, 1, 1, 1, '2019-02-16 06:28:20'),
(74, 2, 63, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(75, 2, 64, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(76, 2, 65, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(77, 2, 66, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(78, 2, 67, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(79, 2, 68, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(80, 2, 69, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(81, 2, 70, 1, 0, 1, 1, '2019-02-26 08:09:40'),
(82, 1, 71, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(83, 1, 72, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(84, 1, 73, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(85, 1, 74, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(86, 1, 75, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(87, 1, 76, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(88, 1, 77, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(89, 1, 78, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(90, 1, 79, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(91, 1, 80, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(92, 1, 81, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(93, 1, 82, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(94, 1, 83, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(95, 1, 84, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(96, 1, 85, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(97, 1, 86, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(98, 1, 87, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(99, 1, 88, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(100, 1, 89, 1, 0, 1, 1, '2019-01-23 00:00:00'),
(101, 1, 90, 1, 1, 1, 1, '2019-01-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_icons`
--

DROP TABLE IF EXISTS `tbl_icons`;
CREATE TABLE `tbl_icons` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_icons`
--

INSERT INTO `tbl_icons` (`id`, `name`, `description`, `created_by`, `create_date`) VALUES
(1, 'fa-500px', 'description', 1, '2018-02-14 22:06:55'),
(2, 'fa-battery-1', '-', 1, '2018-02-14 22:09:37'),
(3, 'fa-battery-empty', '-', 1, '2018-02-14 22:10:42'),
(4, 'fa-battery-three-quarters', '-', 1, '2018-02-14 22:13:07'),
(5, 'fa-calendar-plus-o', '-', 1, '2018-02-14 22:13:35'),
(6, 'fa-chrome', '-', 1, '2018-02-14 22:16:45'),
(7, 'fa-contao', '-', 1, '2018-02-14 22:18:56'),
(8, 'fa-fonticons', '-', 1, '2018-02-14 22:19:40'),
(9, 'fa-gg-circle', '-', 1, '2018-02-14 22:20:30'),
(10, 'fa-hand-peace-o', '-', 1, '2018-02-15 09:17:46'),
(11, 'fa-hand-spock-o', '-', 1, '2018-02-15 09:18:34'),
(12, 'fa-hourglass-2', '-', 1, '2018-02-15 09:19:37'),
(13, 'fa-hourglass-o', '-', 1, '2018-02-17 00:31:49'),
(14, 'fa-industry', '-', 1, '2018-02-18 20:15:10'),
(15, 'fa-map-pin', '-', 1, '2018-02-18 20:17:28'),
(16, 'fa-object-ungroup', '-', 1, '2018-02-18 20:19:00'),
(17, 'fa-opera', '-', 1, '2018-02-18 20:19:53'),
(18, 'fa-sticky-note', '-', 1, '2018-02-18 20:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menus`
--

DROP TABLE IF EXISTS `tbl_menus`;
CREATE TABLE `tbl_menus` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rank` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `module_id` int(32) DEFAULT NULL,
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menus`
--

INSERT INTO `tbl_menus` (`id`, `name`, `path`, `rank`, `level`, `icon`, `module_id`, `is_logged_in`, `is_active`, `description`, `parent_id`, `created_by`, `create_date`) VALUES
(1, 'Home', 'home', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(2, 'About', 'about', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(3, 'Contact', 'contact', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(4, 'Galleries', 'gallery', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(5, 'Events', 'event', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-27 00:00:00'),
(6, 'Login', 'login', 1, 1, '-', 3, 0, 1, '-', 0, 1, '2019-01-27 00:00:00'),
(8, 'Home', '#home', 1, 1, '-', 1, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(9, 'About', '#about', 1, 1, '-', 1, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(10, 'Contact', '#contact', 1, 1, '-', 1, 0, 1, '-', 0, 1, '2019-01-25 00:00:00'),
(11, 'Dashboard', 'dashboard', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(12, 'Profile', 'dashboard/profile', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(13, 'Event', 'dashboard/event', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(14, 'History', 'dashboard/history', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(15, 'News', 'dashboard/news', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(16, 'Socials', 'dashboard/social', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00'),
(17, 'Logout', 'logout', 0, 1, '-', 3, 1, 1, '-', 0, 1, '2019-02-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_method_masters`
--

DROP TABLE IF EXISTS `tbl_method_masters`;
CREATE TABLE `tbl_method_masters` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rank` int(2) NOT NULL,
  `is_mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_method_masters`
--

INSERT INTO `tbl_method_masters` (`id`, `name`, `description`, `rank`, `is_mandatory`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'index', '-', 1, 1, 1, 1, '2018-10-17 00:00:00'),
(2, 'view', '-', 2, 1, 1, 2, '2018-10-17 00:00:00'),
(3, 'insert', '-', 3, 1, 1, 1, '2018-10-17 00:00:00'),
(4, 'remove', '-', 4, 1, 1, 2, '2018-10-17 00:00:00'),
(5, 'delete', '-', 5, 1, 1, 1, '2018-10-17 00:00:00'),
(6, 'dashboard', '-', 6, 0, 1, 1, '2018-10-17 21:39:30'),
(7, 'logout', '-', 7, 0, 1, 1, '2018-10-17 21:40:44'),
(8, 'login', '-', 8, 0, 1, 1, '2018-10-17 21:40:44'),
(9, 'auth', '-', 9, 0, 1, 1, '2018-10-17 21:40:02'),
(10, 'update', '-', 10, 1, 1, 1, '2018-10-17 00:00:00'),
(11, 'update_status', '-', 11, 0, 1, 1, '2018-10-17 00:00:00'),
(12, 'get_list', '-', 12, 1, 1, 1, '2018-10-17 00:00:00'),
(13, 'get_data', '-', 13, 1, 1, 1, '2018-10-17 00:00:00'),
(14, 'get_group', '-', 14, 0, 1, 1, '2018-10-17 00:00:00'),
(15, 'get_method', '-', 15, 0, 1, 1, '2018-10-17 00:00:00'),
(16, 'get_icon', '-', 16, 0, 1, 1, '2018-10-26 09:27:35'),
(17, 'get_menu', '-', 17, 0, 1, 1, '2018-10-29 19:57:39'),
(18, 'task', '-', 18, 0, 1, 1, '2018-11-20 21:27:47'),
(19, 'inbox', '-', 19, 0, 1, 1, '2018-11-20 21:27:58'),
(20, 'rank', '-', 20, 0, 1, 1, '2018-11-24 15:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

DROP TABLE IF EXISTS `tbl_modules`;
CREATE TABLE `tbl_modules` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'frontend', '-', 1, 1, '2019-01-18 00:00:00'),
(2, 'backend', '-', 1, 1, '2019-01-18 00:00:00'),
(3, 'pesky', '-', 1, 1, '2019-01-18 00:00:00'),
(4, 'pos', '-', 1, 1, '2019-01-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permissions`
--

DROP TABLE IF EXISTS `tbl_permissions`;
CREATE TABLE `tbl_permissions` (
  `id` int(32) NOT NULL,
  `module` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`id`, `module`, `class`, `action`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'backend', 'message', 'get_page', '-', 1, 1, '2019-01-15 00:00:00'),
(2, 'backend', 'user', 'login', '-', 1, 1, '2019-01-15 00:00:00'),
(3, 'backend', 'user', 'logout', '-', 1, 1, '2019-01-15 00:00:00'),
(4, 'backend', 'user', 'home', '-', 1, 1, '2019-01-15 00:00:00'),
(5, 'backend', 'user', 'dashboard', '-', 1, 1, '2019-01-15 00:00:00'),
(6, 'backend', 'user', 'my_profile', '-', 1, 1, '2019-01-15 00:00:00'),
(7, 'backend', 'user', 'my_inbox', '-', 1, 1, '2019-01-15 00:00:00'),
(8, 'backend', 'user', 'my_task', '-', 1, 1, '2019-01-15 00:00:00'),
(9, 'backend', 'user', 'lock_screen', '-', 1, 1, '2019-01-15 00:00:00'),
(10, 'backend', 'user', 'get_user_profile', '-', 1, 1, '2019-01-15 00:00:00'),
(11, 'backend', 'user', 'index', '-', 1, 1, '2019-01-15 00:00:00'),
(12, 'backend', 'user', 'view', '-', 1, 1, '2019-01-15 00:00:00'),
(13, 'backend', 'user', 'get_list', '-', 1, 1, '2019-01-15 00:00:00'),
(14, 'backend', 'user', 'get_data', '-', 1, 1, '2019-01-15 00:00:00'),
(15, 'backend', 'user', 'insert', '-', 1, 1, '2019-01-15 00:00:00'),
(16, 'backend', 'user', 'update', '-', 1, 1, '2019-01-15 00:00:00'),
(17, 'backend', 'user', 'update_status', '-', 1, 1, '2019-01-15 00:00:00'),
(18, 'backend', 'user', 'remove', '-', 1, 1, '2019-01-15 00:00:00'),
(19, 'backend', 'user', 'delete', '-', 1, 1, '2019-01-15 00:00:00'),
(20, 'backend', 'group', 'index', '-', 1, 1, '2019-01-15 00:00:00'),
(21, 'backend', 'group', 'view', '-', 1, 1, '2019-01-15 00:00:00'),
(22, 'backend', 'group', 'get_list', '-', 1, 1, '2019-01-15 00:00:00'),
(23, 'backend', 'group', 'get_data', '-', 1, 1, '2019-01-15 00:00:00'),
(24, 'backend', 'group', 'insert', '-', 1, 1, '2019-01-15 00:00:00'),
(25, 'backend', 'group', 'update', '-', 1, 1, '2019-01-15 00:00:00'),
(26, 'backend', 'group', 'update_status', '-', 1, 1, '2019-01-15 00:00:00'),
(27, 'backend', 'group', 'remove', '-', 1, 1, '2019-01-15 00:00:00'),
(28, 'backend', 'group', 'delete', '-', 1, 1, '2019-01-15 00:00:00'),
(29, 'backend', 'group_permission', 'index', '-', 1, 1, '2019-01-15 00:00:00'),
(30, 'backend', 'group_permission', 'view', '-', 1, 1, '2019-01-15 00:00:00'),
(31, 'backend', 'group_permission', 'get_list', '-', 1, 1, '2019-01-15 00:00:00'),
(32, 'backend', 'group_permission', 'get_data', '-', 1, 1, '2019-01-15 00:00:00'),
(33, 'backend', 'group_permission', 'insert', '-', 1, 1, '2019-01-15 00:00:00'),
(34, 'backend', 'group_permission', 'update', '-', 1, 1, '2019-01-15 00:00:00'),
(35, 'backend', 'group_permission', 'update_status', '-', 1, 1, '2019-01-15 00:00:00'),
(36, 'backend', 'group_permission', 'remove', '-', 1, 1, '2019-01-15 00:00:00'),
(37, 'backend', 'group_permission', 'delete', '-', 1, 1, '2019-01-15 00:00:00'),
(38, 'backend', 'group_permission', 'get_group', '-', 1, 1, '2019-01-15 00:00:00'),
(39, 'backend', 'group_permission', 'get_method', '-', 1, 1, '2019-01-15 00:00:00'),
(40, 'backend', 'event', 'index', '-', 1, 1, '2019-02-16 06:28:20'),
(41, 'backend', 'event', 'view', '-', 1, 1, '2019-02-16 06:28:20'),
(42, 'backend', 'event', 'insert', '-', 1, 1, '2019-02-16 06:28:20'),
(43, 'backend', 'event', 'remove', '-', 1, 1, '2019-02-16 06:28:20'),
(44, 'backend', 'event', 'delete', '-', 1, 1, '2019-02-16 06:28:20'),
(45, 'backend', 'event', 'update', '-', 1, 1, '2019-02-16 06:28:20'),
(46, 'backend', 'event', 'get_list', '-', 1, 1, '2019-02-16 06:28:20'),
(47, 'backend', 'event', 'get_data', '-', 1, 1, '2019-02-16 06:28:20'),
(48, 'pesky', 'user', 'login', '-', 1, 1, '2019-01-15 00:00:00'),
(49, 'pesky', 'user', 'logout', '-', 1, 1, '2019-01-15 00:00:00'),
(50, 'pesky', 'user', 'dashboard', '-', 1, 1, '2019-01-15 00:00:00'),
(51, 'pesky', 'user', 'my_profile', '-', 1, 1, '2019-01-15 00:00:00'),
(52, 'pesky', 'user', 'my_inbox', '-', 1, 1, '2019-01-15 00:00:00'),
(53, 'pesky', 'user', 'my_task', '-', 1, 1, '2019-01-15 00:00:00'),
(54, 'pesky', 'user', 'lock_screen', '-', 1, 1, '2019-01-15 00:00:00'),
(55, 'pesky', 'user', 'get_user_profile', '-', 1, 1, '2019-01-15 00:00:00'),
(56, 'pesky', 'user', 'index', '-', 1, 1, '2019-01-15 00:00:00'),
(57, 'pesky', 'user', 'auth', '-', 1, 1, '2019-01-15 00:00:00'),
(58, 'pesky', 'user', 'get_page', '-', 1, 1, '2019-01-15 00:00:00'),
(59, 'pesky', 'user', 'get_data', '-', 1, 1, '2019-01-15 00:00:00'),
(60, 'pesky', 'user', 'home', '-', 1, 1, '2019-01-15 00:00:00'),
(61, 'pesky', 'user', 'about', '-', 1, 1, '2019-01-15 00:00:00'),
(62, 'pesky', 'user', 'contact', '-', 1, 1, '2019-01-15 00:00:00'),
(63, 'backend', 'email_layout', 'index', '-', 1, 1, '2019-02-26 08:09:40'),
(64, 'backend', 'email_layout', 'view', '-', 1, 1, '2019-02-26 08:09:40'),
(65, 'backend', 'email_layout', 'insert', '-', 1, 1, '2019-02-26 08:09:40'),
(66, 'backend', 'email_layout', 'remove', '-', 1, 1, '2019-02-26 08:09:40'),
(67, 'backend', 'email_layout', 'delete', '-', 1, 1, '2019-02-26 08:09:40'),
(68, 'backend', 'email_layout', 'update', '-', 1, 1, '2019-02-26 08:09:40'),
(69, 'backend', 'email_layout', 'get_list', '-', 1, 1, '2019-02-26 08:09:40'),
(70, 'backend', 'email_layout', 'get_data', '-', 1, 1, '2019-02-26 08:09:40'),
(71, 'backend', 'menu', 'index', '-', 1, 1, '2019-01-15 00:00:00'),
(72, 'backend', 'menu', 'view', '-', 1, 1, '2019-01-15 00:00:00'),
(73, 'backend', 'menu', 'get_all_data', '-', 1, 1, '2019-01-15 00:00:00'),
(74, 'backend', 'menu', 'get_data', '-', 1, 1, '2019-01-15 00:00:00'),
(75, 'backend', 'menu', 'get_controller_prefixs', '-', 1, 1, '2019-01-15 00:00:00'),
(76, 'backend', 'menu', 'get_icon', '-', 1, 1, '2019-01-15 00:00:00'),
(77, 'backend', 'menu', 'get_menu', '-', 1, 1, '2019-01-15 00:00:00'),
(78, 'backend', 'menu', 'add_menu', '-', 1, 1, '2019-01-15 00:00:00'),
(79, 'backend', 'menu', 'insert', '-', 1, 1, '2019-01-15 00:00:00'),
(80, 'backend', 'menu', 'insert_form_menu_lv_2', '-', 1, 1, '2019-01-15 00:00:00'),
(81, 'backend', 'menu', 'insert_form_menu_lv_3', '-', 1, 1, '2019-01-15 00:00:00'),
(82, 'backend', 'menu', 'retrieve_menu', '-', 1, 1, '2019-01-15 00:00:00'),
(83, 'backend', 'menu', 'update', '-', 1, 1, '2019-01-15 00:00:00'),
(84, 'backend', 'menu', 'update_status', '-', 1, 1, '2019-01-15 00:00:00'),
(85, 'backend', 'menu', 'remove', '-', 1, 1, '2019-01-15 00:00:00'),
(86, 'backend', 'menu', 'delete', '-', 1, 1, '2019-01-15 00:00:00'),
(87, 'backend', 'menu', 'get_module', '-', 1, 1, '2019-01-15 00:00:00'),
(88, 'backend', 'menu', 'get_menu_2', '-', 1, 1, '2019-01-15 00:00:00'),
(89, 'backend', 'menu', 'get_menu_3', '-', 1, 1, '2019-01-15 00:00:00'),
(90, 'pesky', 'user', 'get_event_list', '-', 1, 1, '2019-01-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_ages`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_ages`;
CREATE TABLE `tbl_pesky_sport_ages` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `level_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_ages`
--

INSERT INTO `tbl_pesky_sport_ages` (`id`, `name`, `description`, `level_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'K1', 'Lahir 1 Januari 2012 atau sesudahnya', 1, 1, 1, '2019-02-13 00:00:00'),
(2, 'K2', 'Lahir 1 Januari 2010 s/d 31 Desember 2011', 1, 1, 1, '2019-02-13 00:00:00'),
(3, 'K3', 'Lahir 1 Januari 2007 s/d 31 Desember 2009', 1, 1, 1, '2019-02-13 00:00:00'),
(4, 'A', 'Lahir 1 Januari 2011 atau sesudahnya', 2, 1, 1, '2019-02-13 00:00:00'),
(5, 'B', 'Lahir 1 Januari 2009 s/d 31 Desember 2010', 2, 1, 1, '2019-02-13 00:00:00'),
(6, 'C', 'Lahir 1 Januari 2007 s/d 31 Desember 2008', 2, 1, 1, '2019-02-13 00:00:00'),
(7, 'D', 'Lahir 1 Januari 2004 s/d 31 Desember 2006', 2, 1, 1, '2019-02-13 00:00:00'),
(8, 'A', 'Lahir 1 Januari 2011 atau sesudahnya', 3, 1, 1, '2019-02-13 00:00:00'),
(9, 'B', 'Lahir 1 Januari 2009 s/d 31 Desember 2010', 3, 1, 1, '2019-02-13 00:00:00'),
(10, 'C', 'Lahir 1 Januari 2007 s/d 31 Desember 2008', 3, 1, 1, '2019-02-13 00:00:00'),
(11, 'D', 'Lahir 1 Januari 2004 s/d 31 Desember 2006', 3, 1, 1, '2019-02-13 00:00:00'),
(12, 'Master', 'Lahir 1 Januari 2003 atau sebelumnya', 3, 1, 1, '2019-02-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_categories`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_categories`;
CREATE TABLE `tbl_pesky_sport_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_categories`
--

INSERT INTO `tbl_pesky_sport_categories` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Sepatu Roda', '-', 1, 1, '2019-02-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_classes`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_classes`;
CREATE TABLE `tbl_pesky_sport_classes` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_classes`
--

INSERT INTO `tbl_pesky_sport_classes` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Pemula', '-', 1, 1, '2019-02-13 00:00:00'),
(2, 'Standar', '-', 1, 1, '2019-02-13 00:00:00'),
(3, 'Speed', '-', 1, 1, '2019-02-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_events`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_events`;
CREATE TABLE `tbl_pesky_sport_events` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `seo_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_start_registration` datetime NOT NULL,
  `date_end_registration` datetime NOT NULL,
  `event_date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_events`
--

INSERT INTO `tbl_pesky_sport_events` (`id`, `name`, `seo_name`, `description`, `date_start_registration`, `date_end_registration`, `event_date`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'test', 'test', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00'),
(2, 'Lari pagi', 'lari-pagi', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00'),
(3, 'Bogor Sport Event', 'bogor-sport-event', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00'),
(4, 'Roda berputar', 'roda-berputar', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00'),
(5, 'Piala sepatu roda walikota Bogor', 'piala-sepatu-roda-walikota-bogor', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00'),
(6, 'Pocari Sweat Run', 'pocari-sweat-run', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', '2019-03-03 00:00:00', '2019-03-31 00:00:00', '2019-04-01 00:00:00', 1, 1, '2019-03-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_event_details`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_event_details`;
CREATE TABLE `tbl_pesky_sport_event_details` (
  `id` int(32) NOT NULL,
  `event_id` int(32) NOT NULL,
  `series_id` int(32) NOT NULL DEFAULT '0',
  `category_id` int(32) NOT NULL,
  `class_id` int(32) NOT NULL,
  `age_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_event_details`
--

INSERT INTO `tbl_pesky_sport_event_details` (`id`, `event_id`, `series_id`, `category_id`, `class_id`, `age_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 0, 1, 1, 4, 1, 1, '2019-03-03 00:00:00'),
(2, 1, 0, 1, 1, 8, 1, 1, '2019-03-03 00:00:00'),
(3, 1, 0, 1, 1, 5, 1, 1, '2019-03-03 00:00:00'),
(4, 1, 0, 1, 1, 9, 1, 1, '2019-03-03 00:00:00'),
(5, 1, 0, 1, 1, 10, 1, 1, '2019-03-03 00:00:00'),
(6, 1, 0, 1, 1, 6, 1, 1, '2019-03-03 00:00:00'),
(7, 1, 0, 1, 1, 7, 1, 1, '2019-03-03 00:00:00'),
(8, 1, 0, 1, 1, 11, 1, 1, '2019-03-03 00:00:00'),
(9, 1, 0, 1, 1, 1, 1, 1, '2019-03-03 00:00:00'),
(10, 1, 0, 1, 1, 2, 1, 1, '2019-03-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_participants`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_participants`;
CREATE TABLE `tbl_pesky_sport_participants` (
  `id` int(32) NOT NULL,
  `first_name` int(100) NOT NULL,
  `last_name` int(155) NOT NULL,
  `birth_date` date NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `gender` int(32) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_participant_clubs`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_participant_clubs`;
CREATE TABLE `tbl_pesky_sport_participant_clubs` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `seo_name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `manager` varchar(255) NOT NULL,
  `coach` varchar(255) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `email` varchar(355) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesky_sport_participant_clubs`
--

INSERT INTO `tbl_pesky_sport_participant_clubs` (`id`, `name`, `seo_name`, `city`, `manager`, `coach`, `phone_number`, `email`, `address`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Monastana DKI Jakarta', 'monastana_dki_jakarta', 'DKI Jakarta', 'Olivia Vergealizi', 'Faisal Norman', '082281953156', 'olivia.virgealizi@mncgroup.com', 'Kwitang', '-', 1, 1, '2019-02-13 00:00:00'),
(2, 'wew', 'wew', '', 'test', '', '', 'firman.begin@gmail.com', '', '', 0, 16, '2019-03-10 20:50:42'),
(3, 'weww', 'weww', '', 'ter', '', '', 'rief.begin@gmail.com', '', '', 0, 3, '2019-03-11 09:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_participant_details`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_participant_details`;
CREATE TABLE `tbl_pesky_sport_participant_details` (
  `id` int(32) NOT NULL,
  `category_id` int(32) NOT NULL,
  `participant_id` int(32) NOT NULL,
  `class_id` int(32) NOT NULL,
  `age_id` int(32) NOT NULL,
  `club_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_series`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_series`;
CREATE TABLE `tbl_pesky_sport_series` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sponsored` text NOT NULL,
  `partner` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesky_sport_venues`
--

DROP TABLE IF EXISTS `tbl_pesky_sport_venues`;
CREATE TABLE `tbl_pesky_sport_venues` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `zoom` int(4) NOT NULL,
  `postcode` varchar(8) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `id` int(32) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `activation_code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 => present, 2 => away, 3 => inactive',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `activation_code`, `status`, `is_active`, `is_logged_in`, `created_by`, `create_date`) VALUES
(1, 'arif', 'arif', 'firman syah', 'firman.begin@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 1, 1, '2019-01-16 00:00:00'),
(2, 'user', 'user', 'oreno', 'admin@admin.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 0, 1, '2019-01-16 00:00:00'),
(3, 'weww', 'weww', 'club', 'rief.begin@gmail.com', '$2y$12$TKmGp.mnRIX3h0RyuZzy/eZtz9ebxfPd3XY68tGLdpJjhvAJErzWO', 'QMyxHjcl1duBmNR853ZwasPLITl6io1e', 3, 0, 0, 1, '2019-03-11 09:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_groups`
--

DROP TABLE IF EXISTS `tbl_user_groups`;
CREATE TABLE `tbl_user_groups` (
  `id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_groups`
--

INSERT INTO `tbl_user_groups` (`id`, `user_id`, `group_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 1, '2019-01-22 00:00:00'),
(2, 1, 2, 1, 1, '2019-01-22 00:00:00'),
(16, 3, 4, 0, 3, '2019-03-11 09:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profiles`
--

DROP TABLE IF EXISTS `tbl_user_profiles`;
CREATE TABLE `tbl_user_profiles` (
  `id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(64) NOT NULL,
  `lng` varchar(64) NOT NULL,
  `zoom` int(4) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ajax_funcs`
--
ALTER TABLE `tbl_ajax_funcs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_categories`
--
ALTER TABLE `tbl_cms_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_category_contents`
--
ALTER TABLE `tbl_cms_category_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_comments`
--
ALTER TABLE `tbl_cms_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_contents`
--
ALTER TABLE `tbl_cms_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_content_photos`
--
ALTER TABLE `tbl_cms_content_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_messages`
--
ALTER TABLE `tbl_component_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_message_categories`
--
ALTER TABLE `tbl_component_message_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_message_labels`
--
ALTER TABLE `tbl_component_message_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_message_status`
--
ALTER TABLE `tbl_component_message_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notifications`
--
ALTER TABLE `tbl_component_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notification_categories`
--
ALTER TABLE `tbl_component_notification_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notification_status`
--
ALTER TABLE `tbl_component_notification_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_tasks`
--
ALTER TABLE `tbl_component_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_task_categories`
--
ALTER TABLE `tbl_component_task_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_task_status`
--
ALTER TABLE `tbl_component_task_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_configs`
--
ALTER TABLE `tbl_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_configs`
--
ALTER TABLE `tbl_email_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_layout`
--
ALTER TABLE `tbl_email_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_links`
--
ALTER TABLE `tbl_email_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_items`
--
ALTER TABLE `tbl_eshop_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_brands`
--
ALTER TABLE `tbl_eshop_item_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_categories`
--
ALTER TABLE `tbl_eshop_item_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_colors`
--
ALTER TABLE `tbl_eshop_item_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_sizes`
--
ALTER TABLE `tbl_eshop_item_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_trans`
--
ALTER TABLE `tbl_eshop_item_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_item_types`
--
ALTER TABLE `tbl_eshop_item_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_masters`
--
ALTER TABLE `tbl_eshop_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_eshop_types`
--
ALTER TABLE `tbl_eshop_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_global_variables`
--
ALTER TABLE `tbl_global_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_groups`
--
ALTER TABLE `tbl_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_group_permissions`
--
ALTER TABLE `tbl_group_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_method_masters`
--
ALTER TABLE `tbl_method_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_ages`
--
ALTER TABLE `tbl_pesky_sport_ages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_categories`
--
ALTER TABLE `tbl_pesky_sport_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_classes`
--
ALTER TABLE `tbl_pesky_sport_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_events`
--
ALTER TABLE `tbl_pesky_sport_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_event_details`
--
ALTER TABLE `tbl_pesky_sport_event_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `age_id` (`age_id`);

--
-- Indexes for table `tbl_pesky_sport_participants`
--
ALTER TABLE `tbl_pesky_sport_participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_participant_clubs`
--
ALTER TABLE `tbl_pesky_sport_participant_clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_participant_details`
--
ALTER TABLE `tbl_pesky_sport_participant_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_id` (`club_id`),
  ADD KEY `age_id` (`age_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `category_id_2` (`category_id`),
  ADD KEY `participant_id` (`participant_id`);

--
-- Indexes for table `tbl_pesky_sport_series`
--
ALTER TABLE `tbl_pesky_sport_series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pesky_sport_venues`
--
ALTER TABLE `tbl_pesky_sport_venues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_groups`
--
ALTER TABLE `tbl_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_profiles`
--
ALTER TABLE `tbl_user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ajax_funcs`
--
ALTER TABLE `tbl_ajax_funcs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cms_categories`
--
ALTER TABLE `tbl_cms_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_category_contents`
--
ALTER TABLE `tbl_cms_category_contents`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_comments`
--
ALTER TABLE `tbl_cms_comments`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_contents`
--
ALTER TABLE `tbl_cms_contents`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_cms_content_photos`
--
ALTER TABLE `tbl_cms_content_photos`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_messages`
--
ALTER TABLE `tbl_component_messages`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_component_message_categories`
--
ALTER TABLE `tbl_component_message_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_message_labels`
--
ALTER TABLE `tbl_component_message_labels`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_message_status`
--
ALTER TABLE `tbl_component_message_status`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_component_notifications`
--
ALTER TABLE `tbl_component_notifications`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_notification_categories`
--
ALTER TABLE `tbl_component_notification_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_notification_status`
--
ALTER TABLE `tbl_component_notification_status`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_tasks`
--
ALTER TABLE `tbl_component_tasks`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_task_categories`
--
ALTER TABLE `tbl_component_task_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_task_status`
--
ALTER TABLE `tbl_component_task_status`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_configs`
--
ALTER TABLE `tbl_configs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_email_configs`
--
ALTER TABLE `tbl_email_configs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_email_layout`
--
ALTER TABLE `tbl_email_layout`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_email_links`
--
ALTER TABLE `tbl_email_links`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_items`
--
ALTER TABLE `tbl_eshop_items`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_brands`
--
ALTER TABLE `tbl_eshop_item_brands`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_categories`
--
ALTER TABLE `tbl_eshop_item_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_colors`
--
ALTER TABLE `tbl_eshop_item_colors`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_sizes`
--
ALTER TABLE `tbl_eshop_item_sizes`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_trans`
--
ALTER TABLE `tbl_eshop_item_trans`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_item_types`
--
ALTER TABLE `tbl_eshop_item_types`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_masters`
--
ALTER TABLE `tbl_eshop_masters`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_eshop_types`
--
ALTER TABLE `tbl_eshop_types`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_global_variables`
--
ALTER TABLE `tbl_global_variables`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_groups`
--
ALTER TABLE `tbl_groups`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_group_permissions`
--
ALTER TABLE `tbl_group_permissions`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_method_masters`
--
ALTER TABLE `tbl_method_masters`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_ages`
--
ALTER TABLE `tbl_pesky_sport_ages`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_categories`
--
ALTER TABLE `tbl_pesky_sport_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_classes`
--
ALTER TABLE `tbl_pesky_sport_classes`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_events`
--
ALTER TABLE `tbl_pesky_sport_events`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_event_details`
--
ALTER TABLE `tbl_pesky_sport_event_details`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_participants`
--
ALTER TABLE `tbl_pesky_sport_participants`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_participant_clubs`
--
ALTER TABLE `tbl_pesky_sport_participant_clubs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_participant_details`
--
ALTER TABLE `tbl_pesky_sport_participant_details`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_series`
--
ALTER TABLE `tbl_pesky_sport_series`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pesky_sport_venues`
--
ALTER TABLE `tbl_pesky_sport_venues`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_user_groups`
--
ALTER TABLE `tbl_user_groups`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_user_profiles`
--
ALTER TABLE `tbl_user_profiles`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_pesky_sport_event_details`
--
ALTER TABLE `tbl_pesky_sport_event_details`
  ADD CONSTRAINT `tbl_pesky_sport_event_details_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `tbl_pesky_sport_events` (`id`),
  ADD CONSTRAINT `tbl_pesky_sport_event_details_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `tbl_pesky_sport_categories` (`id`),
  ADD CONSTRAINT `tbl_pesky_sport_event_details_ibfk_3` FOREIGN KEY (`class_id`) REFERENCES `tbl_pesky_sport_classes` (`id`),
  ADD CONSTRAINT `tbl_pesky_sport_event_details_ibfk_4` FOREIGN KEY (`age_id`) REFERENCES `tbl_pesky_sport_ages` (`id`);

--
-- Constraints for table `tbl_pesky_sport_participant_details`
--
ALTER TABLE `tbl_pesky_sport_participant_details`
  ADD CONSTRAINT `tbl_pesky_sport_participant_details_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `tbl_pesky_sport_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pesky_sport_participant_details_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `tbl_pesky_sport_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pesky_sport_participant_details_ibfk_3` FOREIGN KEY (`participant_id`) REFERENCES `tbl_pesky_sport_participants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pesky_sport_participant_details_ibfk_4` FOREIGN KEY (`age_id`) REFERENCES `tbl_pesky_sport_ages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pesky_sport_participant_details_ibfk_5` FOREIGN KEY (`club_id`) REFERENCES `tbl_pesky_sport_participant_clubs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
