DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `id` int(32) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `activation_code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 => present, 2 => away, 3 => inactive',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `activation_code`, `status`, `is_active`, `is_logged_in`, `created_by`, `create_date`) VALUES
(1, 'arif', 'arif', 'firman syah', 'firman.begin@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 1, 1, '2019-01-16 00:00:00'),
(2, 'user', 'user', 'oreno', 'admin@admin.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 0, 1, '2019-01-16 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;


DELETE FROM `tbl_user_groups` WHERE `tbl_user_groups`.`id` = 1;
DELETE FROM `tbl_user_groups` WHERE `tbl_user_groups`.`id` = 2;
DELETE FROM `tbl_user_groups` WHERE `tbl_user_groups`.`id` = 14;
DELETE FROM `tbl_user_groups` WHERE `tbl_user_groups`.`id` = 15;

ALTER TABLE forums AUTO_INCREMENT = 1;

INSERT INTO `tbl_user_groups` (`id`, `user_id`, `group_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 1, '2019-01-22 00:00:00'),
(2, 1, 2, 1, 1, '2019-01-22 00:00:00');